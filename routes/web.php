<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//antivirus Routes
Route::group(['middleware' => ['web', 'log']], function () {

    Route::get('/', 'IndexController@home')->name('index.index');

    Route::get('{antivirus}/license', 'IndexController@license')->name('menu.license');
    Route::get('{antivirus}/download', 'IndexController@download')->name('menu.download');
    Route::get('{antivirus}/learn', 'IndexController@learn')->name('menu.learn');
    Route::get('{antivirus}/comments', 'IndexController@comments')->name('menu.comments');
    Route::get('{antivirus}/star-comments', 'IndexController@starComments')->name('menu.starComments');
    Route::get('{antivirus}/aboutantivirus', 'IndexController@aboutAntivirus')->name('menu.aboutAntivirus');
    Route::get('{antivirus}/noghate-ghovat', 'IndexController@noghatGhovat')->name('menu.noghatGhovat');
    Route::get('{antivirus}/compare-antivirus', 'IndexController@compareAntivirus')->name('menu.compareAntivirus');

    Route::get('{posts}/post', 'IndexController@post');

    Route::get('upload', 'IndexController@uploadGet')->name('upload.get');
    Route::post('upload', 'IndexController@uploadPost')->name('upload.post');

    Route::get('scaffold-dashboard', 'IndexController@dashboard');

    Route::get('vip/verifym', 'ZarinController@verifyMontyly')->name('payment.verify.monthly');
    Route::get('vip/verifyy', 'ZarinController@verifyYearly')->name('payment.verify.yearly');
    Route::get('vip/price', 'ZarinController@create')->name('payment.buy');
    Route::get('vip/planm', 'ZarinController@planMonthly')->name('payment.monthly');
    Route::get('vip/plany', 'ZarinController@planYearly')->name('payment.yearly');

    Route::get('compare/new', 'IndexController@compareAll')->name('compare.all');
    Route::get('best-antivirus/new', 'IndexController@bestAntivirus')->name('best.antivirus');

    Route::resource('karbar', 'KarbarController');

    Route::match(['GET', 'POST'], 'adminAddVip', 'AdminController@adminAddVip')->name('adminAddVip');

    Route::resource('antivirus', 'AntivirusController');

    Route::resource('post', '\App\Http\Controllers\PostController');
    Route::post('post/{id}/storeCPost', '\App\Http\Controllers\PostController@storeCPost')->name('post.store.cpost');//store posts as comments storeCPost
    Route::patch('post/{id}/updateCPost', '\App\Http\Controllers\PostController@updateCPost')->name('post.update.cpost');//update posts as comments updateCPost
    Route::patch('post/{post}/votePositive', 'PostController@votePositive')->name('post.positive');// like post
    Route::patch('post/{post}/voteNegative', 'PostController@voteNegative')->name('post.negative');// unlike post
    Route::post('post/{id}/update', '\App\Http\Controllers\PostController@update');
    Route::get('post/{id}/delete', '\App\Http\Controllers\PostController@destroy');
    Route::get('post/{id}/deleteMsg', '\App\Http\Controllers\PostController@DeleteMsg');

    Route::resource('category', '\App\Http\Controllers\CategoryController');
    Route::post('category/{id}/update', '\App\Http\Controllers\CategoryController@update');
    Route::get('category/{id}/delete', '\App\Http\Controllers\CategoryController@destroy');
    Route::get('category/{id}/deleteMsg', '\App\Http\Controllers\CategoryController@DeleteMsg');

    Route::resource('comment', '\App\Http\Controllers\CommentController');
    Route::patch('comment/{comment}/editlite', 'CommentController@liteUpdate')->name('comment.lite.update');
    Route::post('comment/{id}/update', '\App\Http\Controllers\CommentController@update');
    Route::get('comment/{id}/delete', '\App\Http\Controllers\CommentController@destroy');
    Route::get('comment/{id}/deleteMsg', '\App\Http\Controllers\CommentController@DeleteMsg');

    Route::resource('license', '\App\Http\Controllers\LicenseController');
    Route::post('license/{id}/update', '\App\Http\Controllers\LicenseController@update');
    Route::get('license/{id}/delete', '\App\Http\Controllers\LicenseController@destroy');
    Route::get('license/{id}/deleteMsg', '\App\Http\Controllers\LicenseController@DeleteMsg');

    Route::resource('tag', '\App\Http\Controllers\TagController');
    Route::post('tag/{id}/update', '\App\Http\Controllers\TagController@update');
    Route::get('tag/{id}/delete', '\App\Http\Controllers\TagController@destroy');
    Route::get('tag/{id}/deleteMsg', '\App\Http\Controllers\TagController@DeleteMsg');

    Route::resource('vote', '\App\Http\Controllers\VoteController');
    Route::post('vote/{id}/update', '\App\Http\Controllers\VoteController@update');
    Route::get('vote/{id}/delete', '\App\Http\Controllers\VoteController@destroy');
    Route::get('vote/{id}/deleteMsg', '\App\Http\Controllers\VoteController@DeleteMsg');

    Route::resource('log', '\App\Http\Controllers\LogController');
    Route::post('log/{id}/update', '\App\Http\Controllers\LogController@update');
    Route::get('log/{id}/delete', '\App\Http\Controllers\LogController@destroy');
    Route::get('log/{id}/deleteMsg', '\App\Http\Controllers\LogController@DeleteMsg');

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('coznet/list', 'CoznetListController@listGet')->name('coznet.get');
    Route::patch('cozne/update/{id}', 'CoznetListController@updateAuto')->name('license.update.auto');
    Route::patch('coznet/list', 'CoznetListController@listPost')->name('coznet.post');
    Route::get('coznet/{id}', 'CoznetController@create')->name('coznet.create');
});


