@extends('scaffold-interface.layouts.app')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
        ویرایش tag
    </h1>
    <a href="{!!url('tag')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Tag </a>
    <br>
    {!! Form::open(['route'=>['tag.update', $tag->id ], 'method' => 'PATCH']) !!} 
    <div class="form-group">
        <label for="name">name</label>
        <input id="name" name = "name" type="text" class="form-control" value="{!!$tag->
        name!!}"> 
    </div>
    <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> بروز رسانی </button>
    {!! Form::close() !!}

</section>
@endsection