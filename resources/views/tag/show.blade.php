@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        نمایش tag
    </h1>
    <br>
    <a href='{!!url("tag")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Tag </a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>کلید واژه</th>
            <th>مقدار</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>name</b> </td>
                <td>{!!$tag->name!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection