@extends('scaffold-interface.layouts.app')
@section('title','Index')
@section('content')

<section class="content">
    <h1>
        انتخاب Tag 
    </h1>
    <a href='{!!url("tag")!!}/create' class = 'btn btn-success'><i class="fa fa-plus"></i> ساختن </a>
    <br>
    <br>
    <table class = "table table-striped table-bordered table-hover" style = 'background:#fff'>
        <thead>
            <th>name</th>
            <th>عملیات</th>
        </thead>
        <tbody>
            @foreach($tags as $tag) 
            <tr>
                <td>{!!$tag->name!!}</td>
                <td>
                    <a data-toggle="modal" data-target="#myModal" class = 'delete btn btn-danger btn-xs' data-link = "/tag/{!!$tag->id!!}/deleteMsg" ><i class = 'fa fa-trash'></i></a>
                    <a href = '#' class = 'viewEdit btn btn-primary btn-xs' data-link = '/tag/{!!$tag->id!!}/edit'><i class = 'fa fa-edit'></i></a>
                    <a href = '#' class = 'viewShow btn btn-warning btn-xs' data-link = '/tag/{!!$tag->id!!}'><i class = 'fa fa-eye'></i></a>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $tags->render() !!}

</section>
@endsection