@extends('scaffold-interface.layouts.app')
@section('title','Create')
@section('content') 
<section class="content">
    <h1>
        ساختن category
    </h1>
    <a href="{!!url('category')!!}" class='btn btn-danger'><i class="fa fa-home"></i> صفحه اصلی Category </a>
    <br>
    {!! Form::open(['route'=>['category.store' ], 'method' => 'post' ]) !!} 
    <div class="form-group $errors->
        has('name') ? ' has-error' : '' "> {!! Form::label('name', 'name:') !!} {!! Form::text('name', null, ['class' => 'form-control']) !!} @if($errors->has("name")) 
        <span class="help-block"><strong> $errors->first("name") </strong></span>
        @endif 
    </div>

    <div class="form-group">
        <label>antiviruses انتخاب </label>
        <select name='antivirus_id' class='form-control'>
            @foreach($antiviruses as $key => $value) 
            <option value="{{$key}}">{{$value}}</option>
            @endforeach 
        </select>
    </div>
    <button class='btn btn-success' type='submit'><i class="fa fa-floppy-o"></i> ذخیره </button>
    {!! Form::close() !!} 
</section>
@endsection