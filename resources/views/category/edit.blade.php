@extends('scaffold-interface.layouts.app')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
        ویرایش category
    </h1>
    <a href="{!!url('category')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Category </a>
    <br>
    {!! Form::open(['route'=>['category.update', $category->id ], 'method' => 'PATCH']) !!}
    <div class="form-group">
        <label for="name">name</label>
        <input id="name" name = "name" type="text" class="form-control" value="{!!$category->
        name!!}">
    </div>
    <div class="form-group">
        <label>antiviruses انتخاب </label>
        <select name = 'antivirus_id' class = "form-control">
            @foreach($antiviruses as $key => $value)
            <option value="{{$key}}">{{$value}}</option>
            @endforeach
        </select>
    </div>
    <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> بروز رسانی </button>
    {!! Form::close() !!}

</section>
@endsection