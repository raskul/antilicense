@extends('scaffold-interface.layouts.app')
@section('title','Index')
@section('content')

<section class="content">
    <h1>
        انتخاب Category 
    </h1>
    <a href='{!!url("category")!!}/create' class = 'btn btn-success'><i class="fa fa-plus"></i> ساختن </a>
    <br>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> مرتبط است با <span class="caret"></span> </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="http://antilicense.dev/antivirus">Antivirus</a></li>
        </ul>
    </div>
    <br>
    <table class = "table table-striped table-bordered table-hover" style = 'background:#fff'>
        <thead>
            <th>نام</th>
            <th>نام آنتی ویروس</th>
            <th>عملیات</th>
        </thead>
        <tbody>
            @foreach($categories as $category) 
            <tr>
                <td>{!!$category->name!!}</td>
                <td>{!!$category->antivirus->name!!}</td>
                <td>
                    <a data-toggle="modal" data-target="#myModal" class = 'delete btn btn-danger btn-xs' data-link = "/category/{!!$category->id!!}/deleteMsg" ><i class = 'fa fa-trash'></i></a>
                    <a href = '#' class = 'viewEdit btn btn-primary btn-xs' data-link = '/category/{!!$category->id!!}/edit'><i class = 'fa fa-edit'></i></a>
                    <a href = '#' class = 'viewShow btn btn-warning btn-xs' data-link = '/category/{!!$category->id!!}'><i class = 'fa fa-eye'></i></a>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $categories->render() !!}

</section>
@endsection