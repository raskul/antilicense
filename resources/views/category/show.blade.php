@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        نمایش category
    </h1>
    <br>
    <a href='{!!url("category")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Category </a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>کلید واژه</th>
            <th>مقدار</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>name</b> </td>
                <td>{!!$category->name!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>name : </i></b>
                </td>
                <td>{!!$category->antivirus->name!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>created_at : </i></b>
                </td>
                <td>{!!$category->antivirus->created_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>updated_at : </i></b>
                </td>
                <td>{!!$category->antivirus->updated_at!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection