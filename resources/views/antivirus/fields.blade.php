<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'نام:') !!}
    {!! Form::text('name', (isset($antivirus)?$antivirus->name:null), ['class' => 'form-control']) !!}
    @if($errors->has("name"))
        <span class="help-block"><strong> {{$errors->first("name")}} </strong></span>
    @endif
</div>

<!-- فیلد نام فارسی -->
<div class="form-group {{ $errors->has('name_fa') ? ' has-error' : '' }}">
    {!! Form::label('name_fa', 'نام فارسی') !!}
    {!! Form::text('name_fa', (isset($antivirus)?$antivirus->name_fa:null), ['class' => 'form-control']) !!}
    @if ($errors->has('name_fa'))
        <span class="help-block"><strong>{{ $errors->first('name_fa') }}</strong></span>
    @endif
</div>

<div class="form-group">
        <label for="">فعال</label>
        <input type="text" class="form-control" value="{{(isset($antivirus)?$antivirus->is_active:null)}}">
</div>
<div class="form-group">
        <label for="">اولویت</label>
        <input type="text" class="form-control" value="{{(isset($antivirus)?$antivirus->order:null)}}">
</div>
<input type="file" name="image">