@extends('scaffold-interface.layouts.app')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
        ویرایش antivirus
    </h1>
    <a href="{!!url('antivirus')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Antivirus </a>
    <br>
    {!! Form::model($antivirus, ['route'=>['antivirus.update', $antivirus->name ], 'method' => 'patch', 'files' => true ]) !!}

    @include('antivirus.fields')

    <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> بروز رسانی </button>
    {!! Form::close() !!}

</section>
@endsection