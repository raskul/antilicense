@extends('scaffold-interface.layouts.app')
@section('title','Create')
@section('content') 
<section class="content">
    <h1>
        ساختن antivirus
    </h1>
    <a href="{!!url('antivirus')!!}" class='btn btn-danger'><i class="fa fa-home"></i> صفحه اصلی Antivirus </a>
    <br>
    {!! Form::open(['route'=>['antivirus.store' ], 'method' => 'post', 'files'=>true ]) !!}

    @include('antivirus.fields')

    <button class='btn btn-success' type='submit'><i class="fa fa-floppy-o"></i> ذخیره </button>
    {!! Form::close() !!} 
</section>
@endsection