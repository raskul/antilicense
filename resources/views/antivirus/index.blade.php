@extends('scaffold-interface.layouts.app')
@section('title','Index')
@section('content')

<section class="content">
    <h1>
        انتخاب Antivirus 
    </h1>
    <a href='{!!url("antivirus")!!}/create' class = 'btn btn-success'><i class="fa fa-plus"></i> ساختن </a>
    <br>
    <br>
    <table class = "table table-striped table-bordered table-hover" style = 'background:#fff'>
        <thead>
            <th>نام</th>
            <th>نام فارسی</th>
            <th>تصویر</th>
            <th>فعال</th>
            <th>اولویت</th>
            <th>عملیات</th>
        </thead>
        <tbody>
            @foreach($antiviruses as $antivirus) 
            <tr>
                <td>{!!$antivirus->name!!}</td>
                <td>{!!$antivirus->name_fa!!}</td>
                <td>{!!$antivirus->image!!}</td>
                <td>{!!$antivirus->is_active!!}</td>
                <td>{!!$antivirus->order!!}</td>
                <td>
                    {!! Form::open(['route'=>['antivirus.destroy', $antivirus->name ], 'method' => 'delete' ]) !!}
                    <button type="submit" class = ' btn btn-danger btn-xs' ><i class = 'fa fa-trash'></i></button>
                    <a href = '{{route('antivirus.edit', $antivirus->name)}}' class = ' btn btn-primary btn-xs' ><i class = 'fa fa-edit'></i></a>
                    <a href = '{{route('antivirus.show', $antivirus->name)}}' class = ' btn btn-warning btn-xs' ><i class = 'fa fa-eye'></i></a>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $antiviruses->render() !!}

</section>
@endsection