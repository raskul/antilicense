@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
آنتی ویروس
        {{ $antivirus->name }}
    </h1>
    <br>
    <a href='{!!url("antivirus")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Antivirus </a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>کلید واژه</th>
            <th>مقدار</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>name</b> </td>
                <td>{!!$antivirus->name!!}</td>
            </tr>
            <tr>
                <td> <b>name</b> </td>
                <td>{!!$antivirus->name_fa!!}</td>
            </tr>
            <tr>
                <td> <b>name</b> </td>
                <td>{!!$antivirus->image!!}</td>
            </tr>
            <tr>
                <td> <b>name</b> </td>
                <td>{!!$antivirus->order!!}</td>
            </tr>
            <tr>
                <td> <b>name</b> </td>
                <td>{!!$antivirus->is_active!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection