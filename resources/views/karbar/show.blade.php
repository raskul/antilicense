@extends('layouts.layout')
@section('content')

    @include('partials.header')

    <br>
    <section class="content">
        <div class="col-sm-12">
            <div class="row" style="margin-top: 50px">
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="row">
                        <img src="{{route('index.index')}}/uploads/images/{{$karbar->image}}" alt="" width="100%"
                             height="100%">
                    </div>
                </div>
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="row">
                        <span>نام:</span>
                        <span>{{ $karbar->name }}</span>
                    </div>

                    <div class="row">
                        <span>امتیاز:</span>
                        <span>{{ $karbar->like }}</span>

                    </div>
                    <div class="row">
                        <span>درباره من :</span>

                    </div>
                    <div class="row">{{ $karbar->about }}</div>
                </div>
                <div class="col-sm-1">
                    @can('is-profile-owner', $karbar->id)
                        <a href="{{route('karbar.edit', $karbar->name)}}" class="btn btn-primary">
                            <span>ویرایش</span>
                            <br>
                            <span>پروفایل</span>
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <br><br><br>

    @include('partials.footer')

@endsection