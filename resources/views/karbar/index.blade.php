@extends('scaffold-interface.layouts.app')
@section('content')
    <section class="content">
        <a href="{{ route('karbar.create') }}" class="btn-primary btn">ساخت یوزر جدید</a>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>نام</th>
                <th>ایمیل</th>
                <th>سمت</th>
                <th>تاریخ عضویت</th>
                <th>عملیات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role->name }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>
                        {!! Form::open(['route' => ['karbar.destroy', $user->name], 'method' => 'delete']) !!}
                        <div class="btn-group">
                            <a href="{!! route('karbar.show', [$user->name]) !!}" class="btn btn-default btn-xs"><i
                                        class="glyphicon glyphicon-eye-open"></i></a>
                            <a href="{!! route('karbar.edit', [$user->name]) !!}" class="btn btn-default btn-xs"><i
                                        class="glyphicon glyphicon-edit"></i></a>
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('ایا اطمینان دارید؟')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br>
        {!! $users->render() !!}

    </section>
@endsection
