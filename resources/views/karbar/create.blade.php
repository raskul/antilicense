@extends('scaffold-interface.layouts.app')
@section('content')
    <section class="content">
        <br>
        <a href="{{ route('karbar.index') }}" class="btn btn-primary">صفحه نخست</a>
        <br>
        <br>

        {!! Form::open(['route'=>['karbar.store'], 'files' => true ]) !!}

        @include('karbar.fields')

        <button type="submit" class="btn btn-success">ثبت</button>
        {!! Form::close() !!}

    </section>

@endsection
