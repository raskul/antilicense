<!-- فیلد نام -->
<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'نام') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    @if ($errors->has('name'))
        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
    @endif
</div>

<!-- فیلد ایمیل -->
<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
    {!! Form::label('email', 'ایمیل') !!}
    @can('is-admin')
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    @else
        {!! Form::text('email', null, ['class' => 'form-control', 'disabled']) !!}
    @endcan
    @if ($errors->has('email'))
        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
    @endif
</div>

<!-- فیلد رمز -->
<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
    {!! Form::label('password', 'رمز') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
    @if ($errors->has('password'))
        <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
    @endif
</div>

<!-- فیلد متن معرفی شما به دیگران : -->
<div class="form-group {{ $errors->has('about') ? ' has-error' : '' }}">
    {!! Form::label('about', 'متن معرفی شما به دیگران ') !!}
    {!! Form::textarea('about', null, ['class' => 'form-control', 'id'=> 'editorcm']) !!}
    @if ($errors->has('about'))
        <span class="help-block"><strong>{{ $errors->first('about') }}</strong></span>
    @endif
</div>

<!-- فیلد سمت -->
<div class="form-group {{ $errors->has('role_id') ? ' has-error' : '' }}">
    {!! Form::label('role_id', 'سمت') !!}
    {!! Form::select('role_id', $role, (isset($user->role_id)?$user->role_id:null), ['class' => 'form-control']) !!}
    @if ($errors->has('role_id'))
        <span class="help-block"><strong>{{ $errors->first('role_id') }}</strong></span>
    @endif
</div>

<!-- فیلد عکس -->
<div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
    {!! Form::label('image', 'عکس') !!}
    {!!  Form::file('image') !!}
    @if ($errors->has('image'))
        <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
    @endif
</div>
@push('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $(".select2").select2({
            dir: "rtl",
            language: "fa",
            width: '100%',
            placeholder: "یکی را انتخاب کنید...",
            tags: false
        });
    </script>

    {{--id textarea -> 'id'=> 'editorcm'--}}
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('textarea#editorcm').ckeditor();
        });
    </script>
@endpush
