{{--@can('is-admin')--}}
{{--@extends('scaffold-interface.layouts.app')--}}
{{--@else--}}
@extends('layouts.layout')
{{--@endcan--}}

@section('content')

    @include('partials.header')

    <section class="container">
        <br>
        @can('is-admin')
            <a href="{{ route('karbar.index') }}" class="btn btn-primary">صفحه نخست</a>
        @else
            <a href="{{ route('index.index') }}" class="btn btn-primary">صفحه نخست</a>
        @endcan
        <br>
        <hr>
        <br>

        {!! Form::model($user, ['route'=>['karbar.update', $user->name ], 'method' => 'patch', 'files' => true ]) !!}

        @include('karbar.fields')

        <button type="submit" class="btn btn-success">بروزرسانی</button>
        {!! Form::close() !!}

    </section>

    <div class="clearfix"></div>
    <br><br><br>

    @include('partials.footer')

@endsection
