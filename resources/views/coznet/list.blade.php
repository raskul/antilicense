@extends('layouts.layout')
@section('content')

    <div class="col-sm-12">
        <div class="row">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>نام</th>
                    <th>نام آنتی ویروس</th>
                    <th>فعال</th>
                </tr>
                </thead>
                <tbody>
                @foreach($lists  as $list)
                    <tr>
                        {!! Form::model($list, ['route'=>['coznet.post', 'id' => $list->id ], 'method' => 'patch' ]) !!}
                        <td>{{ $list->name }}</td>
                        <td><input type="text" name="antivirus_id" value="{{$list->antivirus_id}}"></td>
                        <td><input type="text" name="is_active" value="{{ $list->is_active }}"></td>
                        <td>
                            <button type="submit" class="btn btn-success">بروزرسانی</button>
                        </td>
                        {!! Form::close() !!}
                        <td>
                            <a href="{{ route('coznet.create', $list->antivirus_id) }}" class="btn btn-primary">گرفتن
                                محتوا</a>
                        </td>
                        {!! Form::open(['route'=>['license.update.auto', $list->antivirus_id ], 'method' => 'patch' ]) !!}
                        <td><input type="text" name="content_two_vip"></td>
                        <td>
                            <button type="submit" class="btn btn-warning">بروزرسانی لایسنس</button>
                        </td>
                        {!! Form::close() !!}
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection