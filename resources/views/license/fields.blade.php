<div class="form-group {{$errors->has('content_one') ? ' has-error' : '' }}">
    {!! Form::label('content_one', 'content_one:') !!}
    {!! Form::textarea('content_one', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
    @if($errors->has("content_one"))
        <span class="help-block"><strong> {{$errors->first("content_one")}} </strong></span>
    @endif
</div>
<div class="form-group {{$errors->has('content_one_vip') ? ' has-error' : '' }}">
    {!! Form::label('content_one_vip', 'content_one_vip:') !!}
    {!! Form::textarea('content_one_vip', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
    @if($errors->has("content_one_vip"))
        <span class="help-block"><strong> {{$errors->first("content_one_vip")}} </strong></span>
    @endif
</div>
<div class="form-group {{$errors->has('content_two') ? ' has-error' : '' }}">
    {!! Form::label('content_two', 'content_two:') !!}
    {!! Form::textarea('content_two', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
    @if($errors->has("content_two"))
        <span class="help-block"><strong> {{$errors->first("content_two")}} </strong></span>
    @endif
</div>
<div class="form-group {{$errors->has('content_two_vip') ? ' has-error' : '' }}">
    {!! Form::label('content_two_vip', 'content_two_vip:') !!}
    {!! Form::textarea('content_two_vip', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
    @if($errors->has("content_two_vip"))
        <span class="help-block"><strong> {{$errors->first("content_two_vip")}} </strong></span>
    @endif
</div>
<div class="form-group {{$errors->has('content_three') ? ' has-error' : '' }}">
    {!! Form::label('content_three', 'content_three:') !!}
    {!! Form::textarea('content_three', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
    @if($errors->has("content_three"))
        <span class="help-block"><strong> {{$errors->first("content_three")}} </strong></span>
    @endif
</div>
<div class="form-group {{$errors->has('content_three_vip') ? ' has-error' : '' }}">
    {!! Form::label('content_three_vip', 'content_three_vip:') !!}
    {!! Form::textarea('content_three_vip', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
    @if($errors->has("content_three_vip"))
        <span class="help-block"><strong> {{$errors->first("content_three_vip")}} </strong></span>
    @endif
</div>
<div class="form-group {{$errors->has('content_four') ? ' has-error' : '' }}">
    {!! Form::label('content_four', 'content_four:') !!}
    {!! Form::textarea('content_four', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
    @if($errors->has("content_four"))
        <span class="help-block"><strong> {{$errors->first("content_four")}} </strong></span>
    @endif
</div>
<div class="form-group {{$errors->has('content_four_vip') ? ' has-error' : '' }}">
    {!! Form::label('content_four_vip', 'content_four_vip:') !!}
    {!! Form::textarea('content_four_vip', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
    @if($errors->has("content_four_vip"))
        <span class="help-block"><strong> {{$errors->first("content_four_vip")}} </strong></span>
    @endif
</div>
<div class="form-group {{$errors->has('content_four_vip') ? ' has-error' : '' }}">
    {!! Form::label('content_five', 'content_five:') !!}
    {!! Form::textarea('content_five', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
    @if($errors->has("content_five"))
        <span class="help-block"><strong> {{$errors->first("content_five")}} </strong></span>
    @endif
</div>
<div class="form-group {{$errors->has('modify_user_id') ? ' has-error' : '' }}">
    {!! Form::label('modify_user_id', 'modify_user_id:') !!}
    {!! Form::text('modify_user_id', null, ['class' => 'form-control']) !!} @if($errors->has("modify_user_id"))
        <span class="help-block"><strong> {{$errors->first("modify_user_id")}} </strong></span>
    @endif
</div>

<!-- فیلد آنتی ویروس: -->
<div class="form-group {{ $errors->has('antivirus_id') ? ' has-error' : '' }}">
    {!! Form::label('antivirus_id', 'آنتی ویروس:') !!}
    {!! Form::select('antivirus_id', $antiviruses, (isset($license->antivirus_id)?$license->antivirus_id:null), ['class' => 'form-control']) !!}
    @if ($errors->has('antivirus_id'))
        <span class="help-block"><strong>{{ $errors->first('antivirus_id') }}</strong></span>
    @endif
</div>

@push('footer')
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('textarea#editorcm').ckeditor();
        });
    </script>
@endpush