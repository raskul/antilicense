@extends('scaffold-interface.layouts.app')
@section('title','Create')
@section('content')
<section class="content">
    <h1>
        ساختن license
    </h1>
    <a href="{!!url('license')!!}" class='btn btn-danger'><i class="fa fa-home"></i> صفحه اصلی License </a>
    <br>
    {!! Form::open(['route'=>['license.store' ], 'method' => 'post' ]) !!}

    @include('license.fields')

    <button class='btn btn-success' type='submit'><i class="fa fa-floppy-o"></i> ذخیره </button>
    {!! Form::close() !!}
</section>
@endsection
