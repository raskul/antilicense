@extends('scaffold-interface.layouts.app')
@section('title','Index')
@section('content')

    <section class="content">
        <h1>
            انتخاب License
        </h1>
        <a href='{!!url("license")!!}/create' class='btn btn-success'><i class="fa fa-plus"></i> ساختن </a>
        <br>
        <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="true"> مرتبط است با <span class="caret"></span></button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="http://antilicense.dev/antivirus">Antivirus</a></li>
            </ul>
        </div>
        <br>
        <table class="table table-striped table-bordered table-hover" style='background:#fff'>
            <thead>
            <th >عملیات</th>
            <th >content_one</th>
            <th >modify_user_id</th>
            <th >name</th>
            <th >updated_at</th>
            </thead>
            <tbody>
            @foreach($licenses as $license)
                <tr>
                    <td>
                        <a data-toggle="modal" data-target="#myModal" class='delete btn btn-danger btn-xs'
                           data-link="/license/{!!$license->id!!}/deleteMsg"><i class='fa fa-trash'></i></a>
                        <a href='#' class='viewEdit btn btn-primary btn-xs'
                           data-link='/license/{!!$license->id!!}/edit'><i class='fa fa-edit'></i></a>
                        <a href='#' class='viewShow btn btn-warning btn-xs' data-link='/license/{!!$license->id!!}'><i
                                    class='fa fa-eye'></i></a>
                    </td>
                    <td style="max-width: 200px">{{$license->content_one}}</td>
                    <td>{{$license->modify_user_id}}</td>
                    <td>{{$license->antivirus->name}}</td>
                    <td>{{$license->updated_at}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $licenses->render() !!}

    </section>
@endsection