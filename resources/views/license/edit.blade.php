@extends('scaffold-interface.layouts.app')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
        ویرایش license
    </h1>
    <a href="{!!url('license')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی License </a>
    <br>
    {!! Form::model($license, ['route'=>['license.update', $license->id ], 'method' => 'patch']) !!}

    @include('license.fields')

    <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> بروز رسانی </button>
    {!! Form::close() !!}

</section>
@endsection