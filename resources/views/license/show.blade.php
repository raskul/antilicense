@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        نمایش license
    </h1>
    <br>
    <a href='{!!url("license")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی License </a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>کلید واژه</th>
            <th>مقدار</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>content_one</b> </td>
                <td>{!!$license->content_one!!}</td>
            </tr>
            <tr>
                <td> <b>content_one_vip</b> </td>
                <td>{!!$license->content_one_vip!!}</td>
            </tr>
            <tr>
                <td> <b>content_two</b> </td>
                <td>{!!$license->content_two!!}</td>
            </tr>
            <tr>
                <td> <b>content_two_vip</b> </td>
                <td>{!!$license->content_two_vip!!}</td>
            </tr>
            <tr>
                <td> <b>content_three</b> </td>
                <td>{!!$license->content_three!!}</td>
            </tr>
            <tr>
                <td> <b>content_three_vip</b> </td>
                <td>{!!$license->content_three_vip!!}</td>
            </tr>
            <tr>
                <td> <b>content_four</b> </td>
                <td>{!!$license->content_four!!}</td>
            </tr>
            <tr>
                <td> <b>content_four_vip</b> </td>
                <td>{!!$license->content_four_vip!!}</td>
            </tr>
            <tr>
                <td> <b>modify_user_id</b> </td>
                <td>{!!$license->modify_user_id!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>name : </i></b>
                </td>
                <td>{!!$license->antivirus->name!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>created_at : </i></b>
                </td>
                <td>{!!$license->antivirus->created_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>updated_at : </i></b>
                </td>
                <td>{!!$license->antivirus->updated_at!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection