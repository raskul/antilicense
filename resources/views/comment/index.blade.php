@extends('scaffold-interface.layouts.app')
@section('title','Index')
@section('content')

<section class="content">
    <h1>
        صفحه Comment
    </h1>
    <a href='{!!url("comment")!!}/create' class = 'btn btn-success'><i class="fa fa-plus"></i> ساختن </a>
    <br>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> مرتبط است با <span class="caret"></span> </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="http://antilicense.dev/post">Post</a></li>
            <li><a href="http://antilicense.dev/user">User</a></li>
        </ul>
    </div>
    <br>
    <table class = "table table-striped table-bordered table-hover" style = 'background:#fff'>
        <thead>
            <th>متن دیدگاه</th>
            <th>عنوان پست</th>
            <th>آنتی ویروس</th>
            <th>کاربر</th>
            <th>تاریخ ساخت</th>
            <th>تاریخ ویرایش</th>
            <th width="100px">عملیات</th>
        </thead>
        <tbody>
            @foreach($comments as $comment) 
            <tr>
                <td>{!!$comment->text!!}</td>
                <td>{!!$comment->post->title!!}</td>
                <td>{!! $comment->post->antivirus->name_fa !!}</td>
                <td>{!!$comment->user->name!!}</td>
                <td>{!!$comment->created_at!!}</td>
                <td>{!!$comment->updated_at!!}</td>
                <td>
                    <a data-toggle="modal" data-target="#myModal" class = 'delete btn btn-danger btn-xs' data-link = "/comment/{!!$comment->id!!}/deleteMsg" ><i class = 'fa fa-trash'></i></a>
                    <a href = '#' class = 'viewEdit btn btn-primary btn-xs' data-link = '/comment/{!!$comment->id!!}/edit'><i class = 'fa fa-edit'></i></a>
                    <a href = '#' class = 'viewShow btn btn-warning btn-xs' data-link = '/comment/{!!$comment->id!!}'><i class = 'fa fa-eye'></i></a>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $comments->render() !!}

</section>
@endsection