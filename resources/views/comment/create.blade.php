@extends('scaffold-interface.layouts.app')
@section('title','Create')
@section('content') 
<section class="content">
    <h1>
        ساختن comment
    </h1>
    <a href="{!!url('comment')!!}" class='btn btn-danger'><i class="fa fa-home"></i> صفحه اصلی Comment </a>
    <br>
    {!! Form::open(['route'=>['comment.store' ], 'method' => 'post' ]) !!}

    @include('comment.fields')

    <button class='btn btn-success' type='submit'><i class="fa fa-floppy-o"></i> ذخیره </button>
    {!! Form::close() !!} 
</section>
@endsection