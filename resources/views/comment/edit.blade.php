@extends('scaffold-interface.layouts.app')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
        ویرایش comment
    </h1>
    <a href="{!!url('comment')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Comment </a>
    <br>
    {!! Form::model($comment, ['route'=>['comment.update', $comment->id ], 'method' => 'patch' ]) !!}

    @include('comment.fields')

    <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> بروز رسانی </button>
    {!! Form::close() !!}

</section>
@endsection