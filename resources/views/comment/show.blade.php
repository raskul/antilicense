@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        نمایش comment
    </h1>
    <br>
    <a href='{!!url("comment")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Comment </a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>کلید واژه</th>
            <th>مقدار</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>text</b> </td>
                <td>{!!$comment->text!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>title : </i></b>
                </td>
                <td>{!!$comment->post->title!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>text : </i></b>
                </td>
                <td>{!!$comment->post->text!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>like : </i></b>
                </td>
                <td>{!!$comment->post->like!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>unlike : </i></b>
                </td>
                <td>{!!$comment->post->unlike!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>type : </i></b>
                </td>
                <td>{!!$comment->post->type!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>created_at : </i></b>
                </td>
                <td>{!!$comment->post->created_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>updated_at : </i></b>
                </td>
                <td>{!!$comment->post->updated_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>name : </i></b>
                </td>
                <td>{!!$comment->user->name!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>email : </i></b>
                </td>
                <td>{!!$comment->user->email!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>password : </i></b>
                </td>
                <td>{!!$comment->user->password!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>remember_token : </i></b>
                </td>
                <td>{!!$comment->user->remember_token!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>created_at : </i></b>
                </td>
                <td>{!!$comment->user->created_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>updated_at : </i></b>
                </td>
                <td>{!!$comment->user->updated_at!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection