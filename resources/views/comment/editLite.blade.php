@extends('layouts.layout')
@section('content')

    <section class="content">
        <h1>
            ویرایش
        </h1>
        <a href="{!!url('/')!!}" class='btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی </a>
        <br>
    {!! Form::model($comment, ['route'=>['comment.lite.update', $comment->id ], 'method' => 'patch' ]) !!}

    <!-- فیلد متن -->
        <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
            {!! Form::label('text', 'متن:') !!}
            {!! Form::text('text', null, ['class' => 'form-control']) !!}
            @if ($errors->has('text'))
                <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
            @endif
        </div>

        <button class='btn btn-success' type='submit'><i class="fa fa-floppy-o"></i> بروز رسانی</button>
        {!! Form::close() !!}

    </section>

@endsection