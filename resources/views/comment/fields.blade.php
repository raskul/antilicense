<div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
    {!! Form::label('text', 'متن:') !!}
    {!! Form::text('text', null, ['class' => 'form-control']) !!}
    @if ($errors->has('text'))
        <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
    @endif
</div>
<!-- فیلد پست: -->
<div class="form-group {{ $errors->has('post_id') ? ' has-error' : '' }}">
    {!! Form::label('post_id', 'پست:') !!}
    {!! Form::select('post_id', $posts, (isset($comment->post_id))?$comment->post_id:null, ['class' => 'form-control']) !!}
    @if ($errors->has('post_id'))
        <span class="help-block"><strong>{{ $errors->first('post_id') }}</strong></span>
    @endif
</div>
<!-- فیلد کاربر: -->
<div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
    {!! Form::label('user_id', 'کاربر:') !!}
    {!! Form::select('user_id', $users, (isset($comment->user_id))?$comment->user_id:null, ['class' => 'form-control']) !!}
    @if ($errors->has('user_id'))
        <span class="help-block"><strong>{{ $errors->first('user_id') }}</strong></span>
    @endif
</div>