@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        نمایش log
    </h1>
    <br>
    <a href='{!!url("log")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Log </a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>کلید واژه</th>
            <th>مقدار</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>ip</b> </td>
                <td>{!!$log->ip!!}</td>
            </tr>
            <tr>
                <td> <b>url</b> </td>
                <td>{!!$log->url!!}</td>
            </tr>
            <tr>
                <td> <b>call_back_url</b> </td>
                <td>{!!$log->call_back_url!!}</td>
            </tr>
            <tr>
                <td> <b>os</b> </td>
                <td>{!!$log->os!!}</td>
            </tr>
            <tr>
                <td> <b>browser</b> </td>
                <td>{!!$log->browser!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection