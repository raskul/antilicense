@extends('scaffold-interface.layouts.app')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
        ویرایش log
    </h1>
    <a href="{!!url('log')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Log </a>
    <br>
    {!! Form::open(['route'=>['log.update', $log->id ], 'method' => 'PATCH']) !!} 
    <div class="form-group">
        <label for="ip">ip</label>
        <input id="ip" name = "ip" type="text" class="form-control" value="{!!$log->
        ip!!}"> 
    </div>
    <div class="form-group">
        <label for="url">url</label>
        <input id="url" name = "url" type="text" class="form-control" value="{!!$log->
        url!!}"> 
    </div>
    <div class="form-group">
        <label for="call_back_url">call_back_url</label>
        <input id="call_back_url" name = "call_back_url" type="text" class="form-control" value="{!!$log->
        call_back_url!!}"> 
    </div>
    <div class="form-group">
        <label for="os">os</label>
        <input id="os" name = "os" type="text" class="form-control" value="{!!$log->
        os!!}"> 
    </div>
    <div class="form-group">
        <label for="browser">browser</label>
        <input id="browser" name = "browser" type="text" class="form-control" value="{!!$log->
        browser!!}"> 
    </div>
    <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> بروز رسانی </button>
    {!! Form::close() !!}

</section>
@endsection