@extends('scaffold-interface.layouts.app')
@section('title','Index')
@section('content')

    <section class="content">
        <h1>
            انتخاب Log
        </h1>
        <a href='{!!url("log")!!}/create' class='btn btn-success'><i class="fa fa-plus"></i> ساختن </a>
        <br>
        <br>
        <table class="table table-striped table-bordered table-hover" style='background:#fff'>
            <thead>
            <th>کاربر</th>
            <th>ای پی</th>
            <th>url</th>
            <th>call_back_url</th>
            <th>سیستم عامل</th>
            <th>مرورگر</th>
            <th>زمان</th>
            <th>عملیات</th>
            </thead>
            <tbody>
            @foreach($logs as $log)
                <tr>
                    <td>{!!$log->username!!}</td>
                    <td>{!!$log->ip!!}</td>
                    <td>{!!$log->url!!}</td>
                    <td>{!!$log->call_back_url!!}</td>
                    <td>{!!$log->os!!}</td>
                    <td>{!!$log->browser!!}</td>
                    <td>{!!$log->updated_at!!}</td>
                    <td>
                        <a data-toggle="modal" data-target="#myModal" class='delete btn btn-danger btn-xs'
                           data-link="/log/{!!$log->id!!}/deleteMsg"><i class='fa fa-trash'></i></a>
                        <a href='#' class='viewEdit btn btn-primary btn-xs' data-link='/log/{!!$log->id!!}/edit'><i
                                    class='fa fa-edit'></i></a>
                        <a href='#' class='viewShow btn btn-warning btn-xs' data-link='/log/{!!$log->id!!}'><i
                                    class='fa fa-eye'></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $logs->render() !!}

    </section>
@endsection