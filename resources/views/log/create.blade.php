@extends('scaffold-interface.layouts.app')
@section('title','Create')
@section('content') 
<section class="content">
    <h1>
        ساختن log
    </h1>
    <a href="{!!url('log')!!}" class='btn btn-danger'><i class="fa fa-home"></i> صفحه اصلی Log </a>
    <br>
    {!! Form::open(['route'=>['log.store' ], 'method' => 'post' ]) !!} 
    <div class="form-group $errors->
        has('ip') ? ' has-error' : '' "> {!! Form::label('ip', 'ip:') !!} {!! Form::text('ip', null, ['class' => 'form-control']) !!} @if($errors->has("ip")) 
        <span class="help-block"><strong> $errors->first("ip") </strong></span>
        @endif 
    </div>
    <div class="form-group $errors->
        has('url') ? ' has-error' : '' "> {!! Form::label('url', 'url:') !!} {!! Form::text('url', null, ['class' => 'form-control']) !!} @if($errors->has("url")) 
        <span class="help-block"><strong> $errors->first("url") </strong></span>
        @endif 
    </div>
    <div class="form-group $errors->
        has('call_back_url') ? ' has-error' : '' "> {!! Form::label('call_back_url', 'call_back_url:') !!} {!! Form::text('call_back_url', null, ['class' => 'form-control']) !!} @if($errors->has("call_back_url")) 
        <span class="help-block"><strong> $errors->first("call_back_url") </strong></span>
        @endif 
    </div>
    <div class="form-group $errors->
        has('os') ? ' has-error' : '' "> {!! Form::label('os', 'os:') !!} {!! Form::text('os', null, ['class' => 'form-control']) !!} @if($errors->has("os")) 
        <span class="help-block"><strong> $errors->first("os") </strong></span>
        @endif 
    </div>
    <div class="form-group $errors->
        has('browser') ? ' has-error' : '' "> {!! Form::label('browser', 'browser:') !!} {!! Form::text('browser', null, ['class' => 'form-control']) !!} @if($errors->has("browser")) 
        <span class="help-block"><strong> $errors->first("browser") </strong></span>
        @endif 
    </div>
    <button class='btn btn-success' type='submit'><i class="fa fa-floppy-o"></i> ذخیره </button>
    {!! Form::close() !!} 
</section>
@endsection