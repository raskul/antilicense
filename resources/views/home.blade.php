@extends('layouts.layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ message('خوش آمدید.', 'index.index', 'بازگشت به صفحه اول', 'پیغام سیستم','success' ) }}
                        </div>
                    @else
                        {{ message('شما با موفقیت وارد شدید.', 'index.index', 'بازگشت به صفحه اول', 'پیغام سیستم','success' ) }}
                    @endif
                </div>
                    <div class="panel-footer">
                        <a href="{{ route('index.index') }}">بازگشت به صفحه اصلی سایت.</a>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
