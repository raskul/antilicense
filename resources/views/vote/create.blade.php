@extends('scaffold-interface.layouts.app')
@section('title','Create')
@section('content') 
<section class="content">
    <h1>
        ساختن vote
    </h1>
    <a href="{!!url('vote')!!}" class='btn btn-danger'><i class="fa fa-home"></i> صفحه اصلی Vote </a>
    <br>
    {!! Form::open(['route'=>['vote.store' ], 'method' => 'post' ]) !!} 
    <div class="form-group $errors->
        has('type') ? ' has-error' : '' "> {!! Form::label('type', 'type:') !!} {!! Form::text('type', null, ['class' => 'form-control']) !!} @if($errors->has("type")) 
        <span class="help-block"><strong> $errors->first("type") </strong></span>
        @endif 
    </div>
    <div class="form-group $errors->
        has('user_id') ? ' has-error' : '' "> {!! Form::label('user_id', 'user_id:') !!} {!! Form::text('user_id', null, ['class' => 'form-control']) !!} @if($errors->has("user_id")) 
        <span class="help-block"><strong> $errors->first("user_id") </strong></span>
        @endif 
    </div>
    <div class="form-group $errors->
        has('post_id') ? ' has-error' : '' "> {!! Form::label('post_id', 'post_id:') !!} {!! Form::text('post_id', null, ['class' => 'form-control']) !!} @if($errors->has("post_id")) 
        <span class="help-block"><strong> $errors->first("post_id") </strong></span>
        @endif 
    </div>
    <button class='btn btn-success' type='submit'><i class="fa fa-floppy-o"></i> ذخیره </button>
    {!! Form::close() !!} 
</section>
@endsection