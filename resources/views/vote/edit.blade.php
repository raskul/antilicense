@extends('scaffold-interface.layouts.app')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
        ویرایش vote
    </h1>
    <a href="{!!url('vote')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Vote </a>
    <br>
    {!! Form::open(['route'=>['vote.update', $vote->id ], 'method' => 'PATCH']) !!} 
    <div class="form-group">
        <label for="type">type</label>
        <input id="type" name = "type" type="text" class="form-control" value="{!!$vote->
        type!!}"> 
    </div>
    <div class="form-group">
        <label for="user_id">user_id</label>
        <input id="user_id" name = "user_id" type="text" class="form-control" value="{!!$vote->
        user_id!!}"> 
    </div>
    <div class="form-group">
        <label for="post_id">post_id</label>
        <input id="post_id" name = "post_id" type="text" class="form-control" value="{!!$vote->
        post_id!!}"> 
    </div>
    <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> بروز رسانی </button>
    {!! Form::close() !!}

</section>
@endsection