@extends('scaffold-interface.layouts.app')
@section('title','Index')
@section('content')

<section class="content">
    <h1>
        انتخاب Vote 
    </h1>
    <a href='{!!url("vote")!!}/create' class = 'btn btn-success'><i class="fa fa-plus"></i> ساختن </a>
    <br>
    <br>
    <table class = "table table-striped table-bordered table-hover" style = 'background:#fff'>
        <thead>
            <th>type</th>
            <th>user_id</th>
            <th>post_id</th>
            <th>عملیات</th>
        </thead>
        <tbody>
            @foreach($votes as $vote) 
            <tr>
                <td>{!!$vote->type!!}</td>
                <td>{!!$vote->user_id!!}</td>
                <td>{!!$vote->post_id!!}</td>
                <td>
                    <a data-toggle="modal" data-target="#myModal" class = 'delete btn btn-danger btn-xs' data-link = "/vote/{!!$vote->id!!}/deleteMsg" ><i class = 'fa fa-trash'></i></a>
                    <a href = '#' class = 'viewEdit btn btn-primary btn-xs' data-link = '/vote/{!!$vote->id!!}/edit'><i class = 'fa fa-edit'></i></a>
                    <a href = '#' class = 'viewShow btn btn-warning btn-xs' data-link = '/vote/{!!$vote->id!!}'><i class = 'fa fa-eye'></i></a>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $votes->render() !!}

</section>
@endsection