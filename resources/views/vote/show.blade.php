@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        نمایش vote
    </h1>
    <br>
    <a href='{!!url("vote")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Vote </a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>کلید واژه</th>
            <th>مقدار</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>type</b> </td>
                <td>{!!$vote->type!!}</td>
            </tr>
            <tr>
                <td> <b>user_id</b> </td>
                <td>{!!$vote->user_id!!}</td>
            </tr>
            <tr>
                <td> <b>post_id</b> </td>
                <td>{!!$vote->post_id!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection