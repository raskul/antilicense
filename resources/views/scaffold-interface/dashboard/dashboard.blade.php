@extends('scaffold-interface.layouts.app')
@section('title','Dashboard')
@section('content')
<section class="content-header">
	<h1>
		داشبورد
	</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="icon ">
					<i class="ion ion-person-stalker"></i>
				</div>
				<div class="inner">
					<h3>{{$users}}</h3>
					<p>یوزرها</p>
				</div>

				<a href="{{url('scaffold-users')}}" class="small-box-footer"> اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
			</div>
		</div>
		<!-- list all entities, Development mode-->
		{{--@foreach($entities as $entity)--}}
		{{--<div class="col-lg-3 col-xs-6">--}}
			{{--<!-- small box -->--}}
			{{--<div class="small-box bg-green">--}}
				{{--<div class="inner">--}}
					{{--<h3>{{$entity->tablename}}</h3>--}}
					{{--<p>{{$entity->tablename}}</p>--}}
				{{--</div>--}}
				{{--<div class="icon">--}}
					{{--<i class="fa fa-book"></i>--}}
				{{--</div>--}}
				{{--<a href="{{url('/')}}/{{lcfirst(str_singular($entity->tablename))}}" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>--}}
			{{--</div>--}}
		{{--</div>--}}
		{{--@endforeach--}}
	</div>
</section>
@endsection
