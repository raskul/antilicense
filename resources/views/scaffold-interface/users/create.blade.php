@extends('scaffold-interface.layouts.app')
@section('content')
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3>Create new user</h3>
		</div>
		<div class="box-body">
			<form action="{{route()}}" method = "post">
				{!! Form::open(['route'=>['karbar.store', $user->id ], 'method' => 'post', 'files' => true ]) !!}

				@include('')

				<button class = "btn btn-primary" type="submit">Create</button>
				{!! Form::close() !!}

				<input type="hidden" name = "user_id">

				<button class = "btn btn-primary" type="submit">Create</button>
			</form>
		</div>
	</div>
</section>
@endsection
