<div class="col-sm-6">

    <div class="col-sm-3 ">
        <a href="{{ route('menu.download', $antivirus->name) }}">
            <div class="serviceBox">
                <div class="service-icon">
                    <i class="fa fa-download"></i>
                </div>
                <h5 class="title">دانلود</h5>
            </div>
        </a>
    </div>
    <div class="col-sm-3 ">
        <a href="{{ route('menu.license', $antivirus->name) }}">
            <div class="serviceBox">
                <div class="service-icon">
                    <i class="fa fa-leaf"></i>
                </div>
                <h5 class="title">لایسنس</h5>
            </div>
        </a>
    </div>
    <div class="col-sm-3 ">
        <a href="{{ route('menu.learn', $antivirus->name) }}">
            <div class="serviceBox">
                <div class="service-icon">
                    <i class="fa fa-cubes"></i>
                </div>
                <h5 class="title">آموزش</h5>
            </div>
        </a>
    </div>
    <div class="col-sm-3 ">
        <a href="{{ route('menu.aboutAntivirus', $antivirus->name) }}">
            <div class="serviceBox">
                <div class="service-icon">
                    <i class="fa fa-gears"></i>
                </div>
                <h5 class="title">اطلاعات</h5>
            </div>
        </a>
    </div>

</div>


<div class=" col-sm-6">


    <div class="col-sm-3 ">
        <a href="{{ route('menu.noghatGhovat', $antivirus->name) }}">
            <div class="serviceBox">
                <div class="service-icon">
                    <i class="fa fa-check"></i>
                </div>
                <h5 class="title">نقاط قوت و ضعف</h5>
            </div>
        </a>
    </div>

    <div class="col-sm-3 ">
{{--        <a href="{{ route('menu.compareAntivirus', $antivirus->name) }}">--}}
        <a href="{{ route('compare.all') }}" target="_blank">
            <div class="serviceBox">
                <div class="service-icon">
                    <i class="fa fa-chain"></i>
                </div>
                <h5 class="title">مقایسه</h5>
            </div>
        </a>
    </div>

    <div class="col-sm-3 ">
        <a href="{{ route('menu.comments', $antivirus->name) }}">
            <div class="serviceBox">
                <div class="service-icon">
                    <i class="fa fa-comments"></i>
                </div>
                <h5 class="title">سوالات و دیدگاه ها</h5>
            </div>
        </a>
    </div>

    <div class="col-sm-3 ">
        <a href="{{ route('menu.starComments', $antivirus->name) }}">
            <div class="serviceBox">
                <div class="service-icon">
                    <i class="fa fa-comment-o"></i>
                </div>
                <h5 class="title">دیدگاه های منتخب</h5>
            </div>
        </a>
    </div>


</div>

