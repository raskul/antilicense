<div class="row ">
    <div class="col-sm-12">
        <div class="row answer">


            <div class="col-sm-1 user">
                <div class="row">
                    <img src="{{ route('index.index') }}/uploads/images/{{ $cPost->user->image }}" alt=""
                         width="100%" height="100%">
                </div>
                <div class="row">
                    نام:
                    {{ $cPost->user->name }}
                </div>
                <div class="row">
                    امتیاز:
                    {{ $cPost->user->like }}
                </div>
                <div class="row">
                    <a href="{{route('karbar.show', $cPost->user->name)}}">
                        پروفایل
                    </a>
                </div>
            </div>


            <div class="col-sm-9 ">

                <div class="row">
                    {!! $cPost->text !!}
                    <br>
                    @can('is-post-owner', $cPost)
                        <span class="edit"><a
                                    href="{{ route('post.edit', $cPost->id) }}">ویرایش</a></span>
                    @endcan
                </div>
                @if(count($cPost->comments))
                    <div class="row allcomment">
                        <div class="row">
                            نظر های این دیدگاه:
                        </div>
                        @foreach($cPost->comments as $comment)
                            <div class="comment">
                                <div class="pull-right" style="display: inline">
                                    {{ $comment->user->name }}
                                </div>

                                <div class="" style="display: inline">
                                    <span> : </span>
                                    {{ $comment->text }}
                                    @can('is-comment-owner', $comment)
                                        <span class="edit"><a
                                                    href="{{ route('comment.edit', $comment->id) }}">ویرایش</a></span>
                                    @endcan
                                </div>

                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="col-sm-2">
                <div class="row">
                    {!! Form::open(['route'=>['post.positive', $cPost->id ], 'method' => 'patch' ]) !!}
                    <button type="submit" class="btn btn-success">
                                            <span>
                                                &nbspتشکر&nbsp
                                            </span>
                    </button>
                    {!! Form::close() !!}
                </div>
                <div>

                    @if($cPost->like > 0)
                        <div style="color: green;">
                            {{ $cPost->like }}
                            +
                            نفر از این دیدگاه تشکر کرده اند.
                        </div>
                    @else
                        <div style="font-size: x-small">
                            کسی از دیدگاه شما تشکر نکرده است.
                        </div>
                    @endif
                </div>
                <div class="row">
                    {!! Form::open(['route'=>['post.negative', $cPost->id ], 'method' => 'patch' ]) !!}
                    <button type="submit" class="btn btn-danger">
                                            <span>
                                        گزارش این دیدگاه
                                            </span>
                    </button>
                    {!! Form::close() !!}
                </div>

                <div class="row">

                    @if(Auth::check())
                        <span>
                            <span> نام: </span>
                            <span>
                                {{ auth()->user()->name }}
                            </span>
                        </span>
                        <span>/</span>
                        <span>
                             <a href="#"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">خروج</a>
                             <form id="logout-form" action="/logout" method="POST" style="display: none;">
                                 {{ csrf_field() }}
                             </form>
                        </span>
                        {!! Form::open(['route'=>['comment.store', 'post_id' => $cPost->id ] ]) !!}

                        <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                            {!! Form::label('text', 'نظر:') !!}
                            {!! Form::text('text', null, ['class' => 'form-control']) !!}
                            @if ($errors->has('text'))
                                <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                            @endif
                        </div>

                        <div>
                            <button type="submit" class="btn btn-warning">
                                ثبت <br> نظر
                            </button>
                        </div>

                        {!! Form::close() !!}
                    @else
                        <div>
                            <span>
                                  <a href="{{ route('register') }}">ثبت نام</a>
                            </span>
                            <span>/</span>
                            <span>
                                <a href="{{ route('login') }}">ورود</a>
                            </span>
                        </div>
                    @endif

                </div>

            </div>
        </div>
    </div>
</div>
{{--@endforeach--}}

