@if(Auth::check())
    <p class="alert-warning">
        برای مشاهده محتوا از منوی بالای سایت
        <a href="{{route('payment.buy')}}">
        اکانت ویژه خریداری
        </a>
        نمایید.
    </p>
@else
    <p class="alert-warning">
        برای استفاده از امکانات سایت،
        <a href="{{route('login')}}">
        وارد
        </a>
        شوید یا
        <a href="{{route('register')}}">
        ثبت نام
        </a>
        نمایید.
    </p>
@endif