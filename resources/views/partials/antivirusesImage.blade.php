<div class="content-block gray-bg">
    <div class="container">

        @php
        $i = 0;
        $s = 0.4;
        @endphp
        @foreach($antivirus as $anti)
            @if( $i % 4 == 0 )
                <div class="row">
            @endif

            <div class="col-md-3 col-sm-6">
                <a href="{{ route('menu.license', $anti['name']) }}"
                   class="single-agency-project wow fadeInUp" data-wow-delay="{{$s}}s">
                    <div style="background-image: url(uploads/images/{{ $anti['image'] }})"
                         class="project-bg">
                        <i class="fa fa-hand-pointer-o"></i>
                    </div>
                    <div class="agency-project-info">
                        <h3>{{ $anti['name'] }}</h3>
                        <h3 style="font-size: x-small"> لایسنس {{ $anti['name_fa'] }}</h3>
                         آپدیت شده در تاریخ:
                        @if(isset( $anti->license[0]->updated_at ))
                            @php
                            $date = $anti->license[0]->updated_at;
                            $date_ago = strtotime($date);
                            $diffrence = time() - $date_ago ;
                            $font = 'normal';
                            if ($diffrence < 7852227){
                                $color = 'orangered';
                                if ($diffrence < 5234818){
                                    $font = 'bold';
                                    $color = 'orange';
                                }
                                if($diffrence < 2617409){
                                    $font = 'bold';
                                    $color = 'green';
                                }
                            }else{
                                $color = 'red';
                            }
                            @endphp
                            <span style="color: {{$color}};font-weight: {{$font}} ">
                                {{ \Morilog\Jalali\jDate::forge($anti->license[0]->updated_at)->format('%d %B %Y ') }}
                            </span>
                        @endif
                    </div>
                </a>
            </div>

            @if( $i % 4 == 3 )
                </div>
                <div class="spacer-30"></div>
                @php
                $s = 0.4;
                @endphp
            @endif
            @php
                $i++;
                $s += 0.1;
            @endphp
        @endforeach

        @if( $i % 4 != 4 )
        </div>
        <div class="spacer-30"></div>
        @endif


        {{--<div class="row">--}}

            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<a href="{{ route('menu.license', $antivirus['kaspersky']['id']) }}"--}}
                   {{--class="single-agency-project wow fadeInUp" data-wow-delay="0.5s">--}}
                    {{--<div style="background-image: url(uploads/images/{{ $antivirus['kaspersky']['image'] }})"--}}
                         {{--class="project-bg">--}}
                        {{--<i class="fa fa-hand-pointer-o"></i>--}}
                    {{--</div>--}}
                    {{--<div class="agency-project-info">--}}
                        {{--<h3>{{ $antivirus['kaspersky']['name'] }}</h3>--}}
                        {{--دانلود {{ $antivirus['kaspersky']['name_fa'] }},--}}
                        {{--لایسنس {{ $antivirus['kaspersky']['name_fa'] }},--}}
                        {{--آموزش {{ $antivirus['kaspersky']['name_fa'] }}--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<a href="single-work-agency.html" class="single-agency-project wow fadeInUp" data-wow-delay="0.6s">--}}
                    {{--<div style="background-image: url(assets/img/project-2.jpg)" class="project-bg">--}}
                        {{--<i class="fa fa-link"></i>--}}
                    {{--</div>--}}
                    {{--<div class="agency-project-info">--}}
                        {{--<h3>Lorem ipsum dolor</h3>--}}
                        {{--Web, Graphic--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<a href="single-work-agency.html" class="single-agency-project wow fadeInUp" data-wow-delay="0.7s">--}}
                    {{--<div style="background-image: url(assets/img/project-3.jpg)" class="project-bg">--}}
                        {{--<i class="fa fa-link"></i>--}}
                    {{--</div>--}}
                    {{--<div class="agency-project-info">--}}
                        {{--<h3>Amet voluptatibus</h3>--}}
                        {{--Design, Illustration--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<a href="single-work-agency.html" class="single-agency-project wow fadeInUp" data-wow-delay="0.8s">--}}
                    {{--<div style="background-image: url(assets/img/project-4.jpg)" class="project-bg">--}}
                        {{--<i class="fa fa-link"></i>--}}
                    {{--</div>--}}
                    {{--<div class="agency-project-info">--}}
                        {{--<h3>Iure rerum ducimus</h3>--}}
                        {{--Branding--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}

        {{--</div>--}}

        {{--<div class="spacer-30"></div>--}}

        {{--<div class="row">--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<a href="single-work-agency.html" class="single-agency-project wow fadeInUp" data-wow-delay="0.5s">--}}
                    {{--<div style="background-image: url(assets/img/project-5.jpg)" class="project-bg">--}}
                        {{--<i class="fa fa-link"></i>--}}
                    {{--</div>--}}
                    {{--<div class="agency-project-info">--}}
                        {{--<h3>Officia aut possimus</h3>--}}
                        {{--Illustration--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<a href="single-work-agency.html" class="single-agency-project wow fadeInUp" data-wow-delay="0.6s">--}}
                    {{--<div style="background-image: url(assets/img/project-6.jpg)" class="project-bg">--}}
                        {{--<i class="fa fa-link"></i>--}}
                    {{--</div>--}}
                    {{--<div class="agency-project-info">--}}
                        {{--<h3>Natus provident accusamus</h3>--}}
                        {{--Lorem, Aperiam--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<a href="single-work-agency.html" class="single-agency-project wow fadeInUp" data-wow-delay="0.7s">--}}
                    {{--<div style="background-image: url(assets/img/constraction-recent-work-1.jpg)" class="project-bg">--}}
                        {{--<i class="fa fa-link"></i>--}}
                    {{--</div>--}}
                    {{--<div class="agency-project-info">--}}
                        {{--<h3>Aperiam voluptatibus ut</h3>--}}
                        {{--Provident, Illustration--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<a href="single-work-agency.html" class="single-agency-project wow fadeInUp" data-wow-delay="0.8s">--}}
                    {{--<div style="background-image: url(assets/img/constraction-recent-work-2.jpg)" class="project-bg">--}}
                        {{--<i class="fa fa-link"></i>--}}
                    {{--</div>--}}
                    {{--<div class="agency-project-info">--}}
                        {{--<h3>Quod illo ex modi</h3>--}}
                        {{--Banner, Illustration--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="spacer-30"></div>--}}

        {{--<div class="row">--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<a href="single-work-agency.html" class="single-agency-project wow fadeInUp" data-wow-delay="0.5s">--}}
                    {{--<div style="background-image: url(assets/img/constraction-recent-work-3.jpg)" class="project-bg">--}}
                        {{--<i class="fa fa-link"></i>--}}
                    {{--</div>--}}
                    {{--<div class="agency-project-info">--}}
                        {{--<h3>Dolores voluptates dol</h3>--}}
                        {{--Web, Software--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<a href="single-work-agency.html" class="single-agency-project wow fadeInUp" data-wow-delay="0.6s">--}}
                    {{--<div style="background-image: url(assets/img/constraction-recent-work-4.jpg)" class="project-bg">--}}
                        {{--<i class="fa fa-link"></i>--}}
                    {{--</div>--}}
                    {{--<div class="agency-project-info">--}}
                        {{--<h3>Libero at dolorum</h3>--}}
                        {{--Online, Lorem--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<a href="single-work-agency.html" class="single-agency-project wow fadeInUp" data-wow-delay="0.7s">--}}
                    {{--<div style="background-image: url(assets/img/constraction-recent-work-5.jpg)" class="project-bg">--}}
                        {{--<i class="fa fa-link"></i>--}}
                    {{--</div>--}}
                    {{--<div class="agency-project-info">--}}
                        {{--<h3>Repellendus cumque rem</h3>--}}
                        {{--Libero, Illustration--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<a href="single-work-agency.html" class="single-agency-project wow fadeInUp" data-wow-delay="0.8s">--}}
                    {{--<div style="background-image: url(assets/img/constraction-recent-work-6.jpg)" class="project-bg">--}}
                        {{--<i class="fa fa-link"></i>--}}
                    {{--</div>--}}
                    {{--<div class="agency-project-info">--}}
                        {{--<h3>Logo Design</h3>--}}
                        {{--Branding, Illustration--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}


    </div>
</div>