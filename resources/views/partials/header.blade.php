<!-- Preloader starts -->
<div class="mega-site-preloader-wrap">
    <div class="cont">
        <div class="line square"></div>
        <div class="line square2"></div>
        <div class="line square3"></div>
        <div class="line square4"></div>
    </div>
</div>
<!-- Preloader ends -->


<div class="header-area wow fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="logo">
                    <h1><a href="{{ route('index.index') }}">antilicense.com</a></h1>
                </div>
                <div class="responsive-menu-wrap"></div>
            </div>

            <div class="col-md-9">
                <div class="mainmenu">
                    <ul id="navigation">
                        {{--class="current_page_item"--}}
                        <li><a href="{{ route('index.index') }}">صفحه اصلی</a>
                            {{--<ul>--}}
                            {{--<li><a href="index.html">Homepage Agency</a></li>--}}
                            {{--<li><a href="home-startup.html">Homepage Startup</a></li>--}}
                            {{--<li><a href="home-construction.html">Homepage Construction</a></li>--}}
                            {{--<li><a href="home-education.html">Homepage Education</a></li>--}}
                            {{--<li><a href="home-finance.html">Homepage Finance</a></li>--}}
                            {{--</ul>--}}
                        </li>
                        <li><a href="{{ url('9/post') }}">قوانین سایت</a>
                            {{--<ul>--}}
                            {{--<li><a href="about-agency.html">About Us Agency</a></li>--}}
                            {{--<li><a href="about-startup.html">About Us Startup</a></li>--}}
                            {{--<li><a href="about-construction.html">About Us Construction</a></li>--}}
                            {{--<li><a href="about-education.html">About Us Education</a></li>--}}
                            {{--<li><a href="about-finance.html">About Us Finance</a></li>--}}
                            {{--</ul>--}}
                        </li>
                        <li><a href="{{ url('20/post') }}">تماس با ما</a>
                            {{--<ul>--}}
                            {{--<li><a href="works-agency.html">Works - Agency</a></li>--}}
                            {{--<li><a href="works-startup.html">Works - Startup</a></li>--}}
                            {{--<li><a href="works-construction.html">Works - Construction</a></li>--}}
                            {{--<li><a href="subjects-education.html">Subjects - Education</a></li>--}}
                            {{--<li><a href="works-finance.html">Works - Finance</a></li>--}}
                            {{--</ul>--}}
                        </li>
                        @inject('user','App\classes\helpers\user')
                        @if(Auth::check())
                            @if(isset($user->getUserInfo()->id))
                                <li class="semi-mega-menu"><a href="{{ route('karbar.show', $user->getUserInfo()->name) }}">پروفایل</a>
                                    <ul>
                                        <li>
                                            {{--<span>پروفایل کاربری شما</span>--}}
                                            <ul>
                                                <li>{{ $user->getUserInfo()->name }}
                                                    <span>جان خوش آمدید.</span>
                                                    </li>
                                                <li><a href="{{ route('karbar.edit', $user->getUserInfo()->name) }}">ویرایش پروفایل</a></li>
                                                @php
                                                if (isset($user->getUserInfo()->vip_end_date)){
                                                    if (strtotime($user->getUserInfo()->vip_end_date) > time() ){
                                                        $vip_end_date = \Morilog\Jalali\jDate::forge($user->getUserInfo()->vip_end_date)->format('d M Y');
                                                        $vip_end_date = \Morilog\Jalali\jDateTime::convertNumbers($vip_end_date);
                                                        $vip_end_text = 'تاریخ پایان اکانت ویژه';
                                                        $color = 'green';
                                                    }else{
                                                        $vip_end_date = 'اکانت ویژه شما تمام شده است.';
                                                        $vip_end_text = null;
                                                        $color = 'red';
                                                    }
                                                }else{
                                                    $vip_end_date = 'شما اکانت ویژه ندارید';
                                                    $vip_end_text = null;
                                                    $color = 'orange';
                                                }

                                                    $like = \Morilog\Jalali\jDateTime::convertNumbers($user->getUserInfo()->like);
                                                @endphp
                                                <li><a href="{{ route('index.index') }}/13/post">امتیاز <span> {{ $like }}</span></a></li>
                                                <li><a href="{{ url('vip/price') }}">{{ $vip_end_text }}<span style="color: {{ $color }}">{{ $vip_end_date }}</span></a></li>
                                                <li><a href="{{ url('vip/price') }}">خرید اکانت ویژه</a></li>
                                                <li><a href="#"
                                                       onclick="event.preventDefault();document.getElementById('logoute-form').submit();">خروج</a>
                                                </li>
                                                <form id="logoute-form" action="/logout" method="post"
                                                      style="display: none">{{ csrf_field() }}</form>
                                            </ul>
                                        </li>
                                        <li>
                                            <ul>
                                                <li>
                                                    <div style="width: 200px;height: 200px">
                                                        <img src="{{ route('index.index').'/uploads/images/'.$user->getUserInfo()->image }}"
                                                             alt="" width="200px" height="200px">
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                            <li><a href="{{ url('vip/price') }}">خرید اکانت ویژه</a></li>
                        @else
                            <li><a href="{{ url('register') }}">ثبت نام</a></li>
                            <li><a href="{{ url('login') }}">ورود</a></li>
                        @endif

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>