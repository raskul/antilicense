<div class="footer-area wow fadeIn">
    <div class="container">
        <div class="row">

            <div class="col-md-3 col-sm-6">
                <div class="footer-wid">
                    <p style="font-size: x-large">تماس با ما</p>
                    <p>شماره تماس برای ارسال پیامک یا تماس در مواقع ضروری</p>

                    <p class="big-text"><i class="fa fa-phone"></i> 9031114197 <br/> <i class="fa fa-envelope"></i> support@antilicense.com </p>
                </div>
            </div>
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<div class="footer-wid">--}}
                    {{--<h4>Important Links</h4>--}}
                    {{--<ul>--}}
                        {{--<li><a href="index.html">Homepage</a></li>--}}
                        {{--<li><a href="about-agency.html">About Us</a></li>--}}
                        {{--<li><a href="services-agency.html">Our Services</a></li>--}}
                        {{--<li><a href="works-agency.html">Our Portfolio</a></li>--}}
                        {{--<li><a href="get-a-quote-1.html">Get a FREE quote</a></li>--}}
                        {{--<li><a href="contact-agency.html">Contact Us</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<div class="footer-wid">--}}
                    {{--<h4>Our Sister concerns</h4>--}}
                    {{--<ul>--}}
                        {{--<li><a target="_blank" href="http://themeforest.net">ThemeForest</a></li>--}}
                        {{--<li><a target="_blank" href="http://codecanyon.net">CodeCanyon</a></li>--}}
                        {{--<li><a target="_blank" href="http://graphicriver.net">GraphicRiver</a></li>--}}
                        {{--<li><a target="_blank" href="http://audiojungle.net">AudioJungle</a></li>--}}
                        {{--<li><a target="_blank" href="http://3docean.net">3DOcean</a></li>--}}
                        {{--<li><a target="_blank" href="http://help.envato.com">Envato Help</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-3 col-sm-6">--}}
                {{--<div class="footer-wid">--}}
                    {{--<h4>Contact info</h4>--}}
                    {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, sequi.</p>--}}
                    {{--<p>27 Indira Road, Farmgate<br/>--}}
                        {{--Dhaka 1215</p>--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>

    </div>
</div>