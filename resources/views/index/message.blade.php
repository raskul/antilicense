@extends('layouts.layout')
@section('title')
    پیغام سیستم
@endsection
@section('content')

    @include('partials.header')

    @include('partials.sliderSecond')


    <div class="spacer-10"></div>

    <div class="content-block">
        <div class="container">
            <div class="row">
                <div class="col-md-12 wow fadeInUpBig">

                    <div class="panel panel-{{ $message->type }}">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ $message->title }}</h3>
                        </div>
                        <div class="panel-body">
                            {!! $message->text !!}
                            <br>
                        </div>
                        <div class="panel-footer">
                            @if(isset($route))
                                <a href="{{ route("$route") }}" class="btn btn-default">{{ $buttonText }}</a>
                            @else
                                <button onclick="goBack()" class="btn btn-default">بازگشت</button>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')

@endsection
@push('footer')
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
@endpush