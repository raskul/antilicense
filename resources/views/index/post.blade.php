@extends('layouts.layout')
@section('title')
    @if(isset($post->title))
        {{ $post->title }}
    @else
        antilicense.com
    @endif
@endsection
@push('header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
@endpush
@section('content')

    @include('partials.header')

    @include('partials.sliderSecond')


    <div class="spacer-10"></div>

    <div class="content-block">
        <div class="container">
            <div class="row">
                <div class="col-md-12 wow fadeInUpBig">

                    {!! $post->text !!}

                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')

@endsection
@push('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script>
        $(".select2").select2({
            dir: "rtl",
            language: "fa",
            width: '100%',
            placeholder: "یکی را انتخاب کنید...",
            tags: false
        });
    </script>
@endpush