@extends('layouts.layout')
@section('content')

    @include('partials.header')

    @include('partials.sliderMain')

    @include('partials.antivirusesImage')




    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cta-area wow fadeInUp " >
                    <h3>مقایسه آنتی ویروس ها</h3>
                    <p>مقایسه همه آنتی ویروس ها در سرعت و قدرت</p>
                    <a href="{{ route('compare.all') }}" target="_blank" class="boxed-btn cta-btn">مقایسه کنید</a>
                </div>
            </div>
        </div>
    </div>

    <div class="content-block">
        <div class="container">
            <div class="row">
                <div class="col-md-4 wow fedeInLeft">
                    <h3>لیست بهترین آنتی ویروس های سال</h3>
                    <p>بهترین آنتی ویروس های امسال رو بهتون معرفی میکنیم. مقایسه از نظر قدرت آنتی ویروس و سرعت آن انجام شده...</p>
                    <a href="{{ route('best.antivirus') }}" target="_blank" class="readmore-btn">ادامه مطلب...  <i class="fa fa-angle-left"></i></a>
                </div>
                <div class="col-md-4 text-center wow fadeInUp">
                    <img src="uploads/files/sadegh/1514659381sayer.jpg" alt="" class="img-rounded about-icon-img">
                </div>
                <div class="col-md-4 wow fadeInRight">
                    <h3>کدام آنتی ویروس را انتخاب کنم؟</h3>
                    <p>پیش از این در قسمت مقایسه انتی ویروس های کامپیوتر, به تست و بررسی قدرت و سرعت و … انتی ویروس ها پرداختیم...</p>
                    <a href="{{ route('index.index') }}/مقایسه انتی ویروس ها از نگاه ما/post" target="_blank" class="readmore-btn">ادامه مطلب...  <i class="fa fa-angle-left"></i></a>
                </div>
            </div>
        </div>
    </div>

    <div class="content-block gray-bg padidng-bottom-0 wow fadeIn">
        <img src="assets/img/man.png" class="bg-left" alt="">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-7">
                    <h3>درباره ما</h3>

                    <div class="mega-faqs-wrapper">
                        <div class="mega-single-faq">
                            <p>
                                - دانلود جدیدترین ورژن هر آنتی ویروس
                            </p>
                            <p>
                                - دسترسی به لایسنس هر آنتی ویروس(اعضای ویژه)
                            </p>
                            <p>
                                - آموزش فعال سازی آنتی ویروس ها
                            </p>
                            <p>
                                - آموزش کار کردن با آنتی ویروس ها
                            </p>
                            <p>
                                - پاسخ سریع به مشکلات شما
                            </p>
                            <p>
                                - محلی برای پرسش و پاسخ و بحث و گفت و گو
                            </p>
                            <p>
                                - مقایسه آنتی ویروس ها جهت انتخاب آنتی ویروس مناسب
                            </p>
                            <p>
                                - معرفی بهترین آنتی ویروس های اندروید همراه با لینک دانلود جدیدترین نسخه هر آنتی ویروس
                            </p>
                            {{--<h4>- How you keep my information secure?</h4>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus eveniet sequi placeat beatae neque, officiis repudiandae totam quia, consectetur, soluta adipisci asperiores! Obcaecati unde nihil atque accusamus odit consequuntur suscipit?</p>--}}
                        {{--</div>--}}
                        {{--<div class="mega-single-faq">--}}
                            {{--<h4>- How can I remove information?</h4>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus eveniet sequi placeat beatae neque, officiis repudiandae totam quia, consectetur, soluta adipisci asperiores! Obcaecati unde nihil atque accusamus odit consequuntur suscipit?</p>--}}

                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati reiciendis ut quis laboriosam pariatur repellendus laudantium tempora voluptatem, veniam sequi.</p>--}}
                        {{--</div>--}}
                        {{--<div class="mega-single-faq">--}}
                            {{--<h4>- How do I find my Windows product key?</h4>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus eveniet sequi placeat beatae neque, officiis repudiandae totam quia, consectetur, soluta adipisci asperiores! Obcaecati unde nihil atque accusamus odit consequuntur suscipit?</p>--}}
                        </div>
                    </div>

                    {{--<p class="text-right">Question not listed here? <a href="">Click here ask a question</a></p>--}}
                    {{--<div class="spacer-30"></div>--}}
                </div>
            </div>
        </div>
    </div>

    {{--<div class="content-block wow fadeIn">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<h3 class="text-center">Customers loves us for our work.<br/> Here are some valuable comments.</h3>--}}

                    {{--<div class="testimonial-carousel">--}}
                        {{--<div class="single-testimonial-item">--}}
                            {{--<div class="testimonial-content">--}}
                                {{--<p>In on announcing if of comparison pianoforte projection. Maidsgay yet bed asked blind dried point. On abroad danger likely regretedward do. Too horrible consider followed may differed age.</p>--}}
                            {{--</div>--}}

                            {{--<div class="testimonial-author-info">--}}
                                {{--<img src="assets/img/author.jpg" alt="">--}}
                                {{--<h4>James Cameron</h4>--}}
                                {{--CEO, Open Work Challenge--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="single-testimonial-item">--}}
                            {{--<div class="testimonial-content">--}}
                                {{--<p>In on announcing if of comparison pianoforte projection. Maidsgay yet bed asked blind dried point. On abroad danger likely regretedward do. Too horrible consider followed may differed age.</p>--}}
                            {{--</div>--}}

                            {{--<div class="testimonial-author-info">--}}
                                {{--<img src="assets/img/author-2.jpg" alt="">--}}
                                {{--<h4>John Doe</h4>--}}
                                {{--Marketing Officer, Open Work Challenge--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="single-testimonial-item">--}}
                            {{--<div class="testimonial-content">--}}
                                {{--<p>In on announcing if of comparison pianoforte projection. Maidsgay yet bed asked blind dried point. On abroad danger likely regretedward do. Too horrible consider followed may differed age.</p>--}}
                            {{--</div>--}}

                            {{--<div class="testimonial-author-info">--}}
                                {{--<img src="assets/img/author.jpg" alt="">--}}
                                {{--<h4>James Cameron</h4>--}}
                                {{--CEO, Open Work Challenge--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}



    <div class="content-block">
        <div class="container">

            @php
                $i = 0;
            @endphp
            @foreach($posts as $post)
                @if($i % 3 == 0)
                    <div class="row">
                @endif
                    <div class="col-sm-4">
                        <div class="single-service-item wow fadeInUp" style="border-top: 3px solid #1360F4">
                            @if(isset($post->type->name))
                                <span style="font-size: x-large ">{{ $post->type->name }} - {{ $post->antivirus->name }}</span>
                            @else
                                <span style="font-size: x-large ">دیدگاه</span>
                            @endif
                            <p>{!! strip_tags(substr($post->text, 0, 100)) !!}</p>
                            <a href="/{{ $post->id }}/post" class="readmore-btn">ادامه مطلب...  <i class="fa fa-angle-left"></i></a>
                        </div>
                    </div>
                @if($i % 3 == 2)
                    </div>
                @endif
                @php
                    $i++;
                @endphp
            @endforeach
            @if($i % 3 != 3)
                </div>
            @endif

                {{--<div class="col-sm-4">--}}
                    {{--<div class="single-service-item wow fadeInUp" data-wow-delay="1.1s">--}}
                        {{--<h3>fdgfdgfdgfdgfdgfdg</h3>--}}
                        {{--<p>fgdfgdfgdfsgfdgsdfgfgdfsgfdg.</p>--}}
                        {{--<a href="/11/post" class="readmore-btn">ادامه مطلب...  <i class="fa fa-angle-left"></i></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-4">--}}
                    {{--<div class="single-service-item wow fadeInUp" data-wow-delay="1.2s">--}}
                        {{--<h3>App Development</h3>--}}
                        {{--<p>Our awesome members are ready to create your dream website project with passion and dedication.</p>--}}
                        {{--<a href="single-service-agency.html" class="readmore-btn">ادامه مطلب...  <i class="fa fa-angle-left"></i></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="specer-70 hidden-xs"></div>--}}
            {{--<div class="row">--}}
                {{--<div class="col-sm-4">--}}
                    {{--<div class="single-service-item wow fadeInUp" data-wow-delay="1.3s">--}}
                        {{--<h3>Social Media Marketing</h3>--}}
                        {{--<p>Our awesome members are ready to create your dream website project with passion and dedication.</p>--}}
                        {{--<a href="single-service-agency.html" class="readmore-btn">ادامه مطلب...  <i class="fa fa-angle-left"></i></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-4">--}}
                    {{--<div class="single-service-item wow fadeInUp" data-wow-delay="1.4s">--}}
                        {{--<h3>SEO Services</h3>--}}
                        {{--<p>Our awesome members are ready to create your dream website project with passion and dedication.</p>--}}
                        {{--<a href="single-service-agency.html" class="readmore-btn">ادامه مطلب...  <i class="fa fa-angle-left"></i></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-4">--}}
                    {{--<div class="single-service-item wow fadeInUp" data-wow-delay="1.5s">--}}
                        {{--<h3>Content Writing</h3>--}}
                        {{--<p>Our awesome members are ready to create your dream website project with passion and dedication.</p>--}}
                        {{--<a href="single-service-agency.html" class="readmore-btn">ادامه مطلب...  <i class="fa fa-angle-left"></i></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        <div class="clearfix"></div>
        <br>
        <div class="col-sm-10 col-sm-offset-2">
            {{ $posts->render() }}
        </div>

    </div>
    </div>


    @include('partials.footer')

@endsection
