@extends('layouts.layout')
@section('content')

    @if(!Auth::check())
        {{ message('ابتدا وارد حساب کاربری خود شوید.', 'login', 'هدایت به صفحه ورود', 'پیغام سیستم', 'danger') }}
    @else
        @can('is-writer')

            @include('partials.header')

            <a href="{{ route('upload.get') }}" class="btn btn-link">صفحه آپلود</a>
            <br><br><br><br><br>

            {!! Form::open(['route'=>['upload.post' ], 'method' => 'post', 'files' => true ]) !!}

            <input type="file" name="file1">
            @if(isset($url1))
                <div>{{ $url1 }}</div>@endif
            <br>
            <input type="file" name="file2">
            @if(isset($url2))
                <div>{{ $url2 }}</div>@endif
            <br>
            <input type="file" name="file3">
            @if(isset($url3))
                <div>{{ $url3 }}</div>@endif
            <br>
            <input type="file" name="file4">
            @if(isset($url4))
                <div>{{ $url4 }}</div>@endif
            <br>
            <input type="file" name="file5">
            @if(isset($url5))
                <div>{{ $url5 }}</div>@endif
            <br>
            <button type="submit" class="btn btn-primary">آپلود</button>

            {!! Form::close() !!}

            <br><br><br><br><br>
            @include('partials.footer')

        @endcan
    @endif
@endsection