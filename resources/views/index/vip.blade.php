@extends('layouts.layout')
@section('content')

    @include('partials.header')

    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <br>
                <span style="font-size: xx-large; color: black">قیمت های ما</span>
                <br>
                <span>با خرید اکانت ویژه از ما حمایت کنید.</span>
                <br><br><br>
            </div>
        </div>

        @if (Auth::check())

        <div class="row">
            <div class="col-sm-4 col-sm-offset-2">
                <div class="mega-pricing-item gradient-style-1">
                    <div class="pricing-icon">
                        <i class="fa fa-automobile"></i>
                    </div>
                    <h3><strong>عضویت ویژه ماهیانه</strong></h3>
                    <ul>
                        <li>درسترسی به بخش
                            <br>
                            <strong>
                                لایسنس ها
                            </strong>
                            <br>
                            به مدت یک ماه</li>
                    </ul>
                    <h1><span>تومان</span>1000</h1>
                    <p class="pricing-duration">ماهیانه</p>
                    <a href="{{ route('payment.monthly') }}" class="bordered-btn">خرید</a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="mega-pricing-item gradient-style-2 active">
                    <div class="pricing-icon">
                        <i class="fa fa-plane"></i>
                    </div>
                    <h3><strong>عضویت ویژه سالیانه</strong></h3>
                    <ul>
                        <li>درسترسی به بخش
                            <br>
                            <strong>
                                لایسنس ها
                            </strong>
                            <br>
                            به مدت یک سال</li>
                    </ul>
                    <h1><span> تومان </span>5000</h1>
                    <p class="pricing-duration">سالیانه</p>
                    <a href="{{ route('payment.yearly') }}" class="boxed-btn">خرید</a>
                </div>
            </div>
            {{--<div class="col-sm-4">--}}
                {{--<div class="mega-pricing-item gradient-style-3">--}}
                    {{--<div class="pricing-icon">--}}
                        {{--<i class="fa fa-grav"></i>--}}
                    {{--</div>--}}
                    {{--<h3><strong>عضویت ویژه</strong> ماهیانه</h3>--}}


                    {{--<ul>--}}
                        {{--<li>درسترسی به بخش--}}
                            {{--<br>--}}
                            {{--<strong>--}}
                            {{--لایسنس ها--}}
                            {{--</strong>--}}
                            {{--<br>--}}
                            {{--به مدت یک ماه</li>--}}
                    {{--</ul>--}}
                    {{--<h1><span>تومان</span>1000</h1>--}}
                    {{--<p class="pricing-duration">ماهیانه</p>--}}
                    {{--<a href="" class="bordered-btn">خرید</a>--}}
                {{--</div>--}}
            {{--</div>--}}

            @else
                <div>برای خرید اکانت ویژه ابتدا وارد اکانت خود شوید و یا ثبت نام کنید.</div>
            @endif

        </div>
    </div>

    <br>
    <br>
    <br>


    @include('partials.footer')

@endsection