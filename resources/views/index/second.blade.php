@extends('layouts.layout')
@section('title')
    {{ $title }}
    {{ $antivirus->name_fa }}
@endsection
@section('content')

    @include('partials.header')

    @include('partials.sliderSecond')

    <div class="spacer-10"></div>
    <div class="spacer-10"></div>
    <hr>
    <div style="font-size: x-large">
        منوی اصلی آنتی ویروس
        {{ $antivirus->name_fa }}:
    </div>


    @include('partials.serviceBox')


    <div class="spacer-100"></div>
    <div class="spacer-100"></div>

    @if($title == 'لایسنس')

        @can('is-editor')
            <a href="{{ route('license.edit', $text['id']) }}">ویرایش</a>
        @endcan

        <div class="content-block">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInRight">
                        <div class="spacer-100"></div>
                        <h3>{{ $title }} {{ $antivirus->name_fa }}<br/>{{ $antivirus->name }}</h3>
                        <div class="spacer-10"></div>

                        @if(isset($text['content_one']))
                            <div>{!! $text['content_one'] !!}</div>
                        @endif
                        @if(isset($text['content_one_vip']))
                            @can('is-vip')
                                <div style="border-bottom: 1px dashed #0b0b0b">{!! $text['content_one_vip'] !!}</div>
                            @else
                                @include('partials.notLoginVip')
                            @endcan
                        @endif
                        @if(isset($text['content_two']))
                            <div>{!! $text['content_two'] !!}</div>
                        @endif
                        @if(isset($text['content_two_vip']))
                            @can('is-vip')
                                <div style="border-bottom: 1px dashed #0b0b0b">{!! $text['content_two_vip'] !!}</div>
                            @else
                                @include('partials.notLoginVip')
                            @endcan
                        @endif
                        @if(isset($text['content_three']))
                            <div>{!! $text['content_three'] !!}</div>
                        @endif
                        @if(isset($text['content_three_vip']))
                            @can('is-vip')
                                <div style="border-bottom: 1px dashed #0b0b0b">{!! $text['content_three_vip'] !!}</div>
                            @else
                                @include('partials.notLoginVip')
                            @endcan
                        @endif
                        @if(isset($text['content_four']))
                            <div>{!! $text['content_four'] !!}</div>
                        @endif
                        @if(isset($text['content_four_vip']))
                            @can('is-vip')
                                <div style="border-bottom: 1px dashed #0b0b0b">{!! $text['content_four_vip'] !!}</div>
                            @else
                                @include('partials.notLoginVip')
                            @endcan
                        @endif
                        @if(isset($text['content_five']))
                            <div style="border-bottom: 1px dashed #0b0b0b">{!! $text['content_five'] !!}</div>
                        @endif

                        <div class="spacer-20"></div>
                    </div>

                    <div class="col-md-6 wow fadeInLeft">
                        <img src="{{ route('index.index') }}/uploads/images/{{ $antivirus->image }}" alt="">
                    </div>
                </div>
            </div>
        </div>

    @elseif($title == 'دیدگاه های' || $title == 'دیدگاه های منتخب')

        <div class="container">
            @foreach($text as $cPost)

                @include('partials.comment')

            @endforeach
        </div>

        @if($title == 'دیدگاه های' && Auth::check())
            <div class="container">
            {!! Form::open(['route'=>['post.store.cpost', $antivirus->id ] ]) !!}
            <!-- فیلد متن: -->
                <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                    {!! Form::label('text', 'متن:') !!}
                    {!! Form::textarea('text', null, ['class' => 'form-control', 'id'=>'editorcm']) !!}
                    @if ($errors->has('text'))
                        <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">ثبت دیدگاه</button>
                {!! Form::close() !!}
            </div>
        @endif

    @else
        @if(isset($text))
        @can('is-editor')
            <a href="{{ route('post.edit', $text->id) }}">ویرایش</a>
        @endcan

        <div class="content-block">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInRight">
                        <div class="spacer-100"></div>
                        <h3>{{ $title }} {{ $antivirus->name_fa }}<br/>{{ $antivirus->name }}</h3>
                        <div class="spacer-10"></div>

                        {!! $text->text !!}

                        <div class="spacer-20"></div>
                    </div>

                    <div class="col-md-6 wow fadeInLeft">
                        <img src="{{ route('index.index') }}/uploads/images/{{ $antivirus->image }}" alt="">
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="content-block">
                <div class="container">
                    <div class="row">
                        در حال حاضر هیچ پستی وجود ندارد.
                    </div>
                </div>
            </div>
        @endif

    @endif

    <div class="spacer-10"></div>
    <div class="spacer-10"></div>

    @include('partials.footer')

@endsection

@if($title == 'دیدگاه های')
    @push('footer')
        <script type="text/javascript" src="{{ asset('editor/basic/ckeditor/ckeditor.js') }}"></script>
        <script type="text/javascript" src="{{ asset('editor/basic/ckeditor/adapters/jquery.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('textarea#editorcm').ckeditor();
            });
        </script>
    @endpush
@endif