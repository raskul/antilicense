@extends('scaffold-interface.layouts.app')
@section('title','Index')
@section('content')

    <section class="content">
        <h1>
            انتخاب Post
        </h1>
        <a href='{!!url("post")!!}/create' class='btn btn-success'><i class="fa fa-plus"></i> ساختن </a>
        <br>
        <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="true"> مرتبط است با <span class="caret"></span></button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="http://antilicense.dev/category">Category</a></li>
                <li><a href="http://antilicense.dev/antivirus">Antivirus</a></li>
                <li><a href="http://antilicense.dev/user">User</a></li>
            </ul>
        </div>
        <br>
        <table class="table table-striped table-bordered table-hover" style='background:#fff'>
            <thead>
            <th width="100px">عملیات</th>
            <th>عنوان</th>
            <th>متن</th>
            <th>نوع</th>
            <th>دسته بندی</th>
            <th>آنتی ویروس</th>
            <th>کاربر</th>
            <th>تاریخ</th>
            </thead>
            <tbody>
            @foreach($posts as $post)
                <tr>
                    <td>
                        <a data-toggle="modal" data-target="#myModal" class='delete btn btn-danger btn-xs'
                           data-link="/post/{!!$post->id!!}/deleteMsg"><i class='fa fa-trash'></i></a>
                        <a href='#' class='viewEdit btn btn-primary btn-xs' data-link='/post/{!!$post->id!!}/edit'><i
                                    class='fa fa-edit'></i></a>
                        <a href='#' class='viewShow btn btn-warning btn-xs' data-link='/post/{!!$post->id!!}'><i
                                    class='fa fa-eye'></i></a>
                    </td>
                    <td>{!!$post->title!!}</td>
                    <td>{{$post->text}}</td>
                    <td>{!!$post->type->name!!}</td>
                    <td>
                        @if(isset($post->category->name))
                            {!!$post->category->name!!}
                        @endif
                    </td>
                    <td>{!!$post->antivirus->name!!}</td>
                    <td>{!!$post->user->name!!}</td>
                    <td>{!!$post->updated_at!!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $posts->render() !!}

    </section>
@endsection
@push('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script>
        $(".select2").select2({
            dir: "rtl",
            language: "fa",
            width: '100%',
            placeholder: "یکی را انتخاب کنید...",
            tags: false
        });
    </script>
@endpush