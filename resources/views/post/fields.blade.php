<div class="form-group {{$errors->has('title') ? ' has-error' : ''}} ">
    {!! Form::label('title', 'عنوان:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
    @if($errors->has("title"))
        <span class="help-block"><strong> {{$errors->first("title") }}</strong></span>
    @endif
</div>
<div class="form-group {{$errors->has('text') ? ' has-error' : ''}} ">
    {!! Form::label('text', 'متن:') !!}
    {!! Form::textarea('text', null, ['class' => 'form-control', 'id'=>'editorcm']) !!}
    @if($errors->has("text"))
        <span class="help-block"><strong> {{$errors->first("text")}} </strong></span>
    @endif
</div>
<!-- فیلد نوع: -->
<div class="form-group {{ $errors->has('type_id') ? ' has-error' : '' }}">
    {!! Form::label('type_id', 'نوع:') !!}
    {!! Form::select('type_id', $types, (isset($post->type_id)?$post->type_id:null), ['class' => 'form-control']) !!}
    @if ($errors->has('type_id'))
        <span class="help-block"><strong>{{ $errors->first('type_id') }}</strong></span>
    @endif
</div>
<!-- فیلد برچسب ها -->
<div class="form-group {{ $errors->has('tag_id[]') ? ' has-error' : '' }}">
    {!! Form::label('tag_id[]', 'برچسب ها') !!}
    {!! Form::select('tag_id[]',$tags , (isset($post->tags)?$post->tags:null), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
    @if ($errors->has('tag_id[]'))
        <span class="help-block"><strong>{{ $errors->first('tag_id[]') }}</strong></span>
    @endif
</div>
<!-- فیلد دسته -->
<div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
    {!! Form::label('category_id', 'دسته') !!}
    {!! Form::select('category_id', $categories, (isset($post))?$post->category_id:null, ['class' => 'form-control']) !!}
    @if ($errors->has('category_id'))
        <span class="help-block"><strong>{{ $errors->first('category_id') }}</strong></span>
    @endif
</div>
<!-- فیلد آنتی ویروس -->
<div class="form-group {{ $errors->has('antivirus_id') ? ' has-error' : '' }}">
    {!! Form::label('antivirus_id', 'آنتی ویروس') !!}
    {!! Form::select('antivirus_id', $antiviruses, (isset($post))?$post->antivirus_id:null, ['class' => 'form-control']) !!}
    @if ($errors->has('antivirus_id'))
        <span class="help-block"><strong>{{ $errors->first('antivirus_id') }}</strong></span>
    @endif
</div>
<!-- فیلد کاربر -->
<div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
    {!! Form::label('user_id', 'کاربر') !!}
    {!! Form::select('user_id', $users, (isset($post))?$post->user_id:null, ['class' => 'form-control']) !!}
    @if ($errors->has('user_id'))
        <span class="help-block"><strong>{{ $errors->first('user_id') }}</strong></span>
    @endif
</div>
@push('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script>
        $(".select2").select2({
            dir: "rtl",
            language: "fa",
            width: '100%',
            placeholder: "یکی را انتخاب کنید...",
            tags: false
        });
    </script>

    {{--id textarea -> 'id'=> 'editorcm'--}}
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('textarea#editorcm').ckeditor();
        });
    </script>



@endpush