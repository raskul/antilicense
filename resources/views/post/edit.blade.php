@extends('scaffold-interface.layouts.app')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
        ویرایش post
    </h1>
    <a href="{!!url('post')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Post </a>
    <br>
    {!! Form::model($post, ['route'=>['post.update', $post->id ], 'method' => 'patch' ]) !!}

    @include('post.fields')

    <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> بروز رسانی </button>
    {!! Form::close() !!}

</section>
@endsection