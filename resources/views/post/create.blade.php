@extends('scaffold-interface.layouts.app')
@section('title','Create')
@section('content') 
<section class="content">
    <h1>
        ساختن post
    </h1>
    <a href="{!!url('post')!!}" class='btn btn-danger'><i class="fa fa-home"></i> صفحه اصلی Post </a>
    <br>
    {!! Form::open(['route'=>['post.store' ], 'method' => 'post' ]) !!} 

    @include('post.fields')

    <button class='btn btn-success' type='submit'><i class="fa fa-floppy-o"></i> ذخیره </button>
    {!! Form::close() !!}

</section>

@endsection
