@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        نمایش post
    </h1>
    <br>
    <a href='{!!url("post")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی Post </a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>کلید واژه</th>
            <th>مقدار</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>title</b> </td>
                <td>{!!$post->title!!}</td>
            </tr>
            <tr>
                <td> <b>text</b> </td>
                <td>{!!$post->text!!}</td>
            </tr>
            <tr>
                <td> <b>like</b> </td>
                <td>{!!$post->like!!}</td>
            </tr>
            <tr>
                <td> <b>unlike</b> </td>
                <td>{!!$post->unlike!!}</td>
            </tr>
            <tr>
                <td> <b>type</b> </td>
                <td>{!!$post->type!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>name : </i></b>
                </td>
                <td>{!!$post->category->name!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>type : </i></b>
                </td>
                <td>{!!$post->category->type!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>created_at : </i></b>
                </td>
                <td>{!!$post->category->created_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>updated_at : </i></b>
                </td>
                <td>{!!$post->category->updated_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>name : </i></b>
                </td>
                <td>{!!$post->antivirus->name!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>created_at : </i></b>
                </td>
                <td>{!!$post->antivirus->created_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>updated_at : </i></b>
                </td>
                <td>{!!$post->antivirus->updated_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>name : </i></b>
                </td>
                <td>{!!$post->user->name!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>email : </i></b>
                </td>
                <td>{!!$post->user->email!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>password : </i></b>
                </td>
                <td>{!!$post->user->password!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>remember_token : </i></b>
                </td>
                <td>{!!$post->user->remember_token!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>created_at : </i></b>
                </td>
                <td>{!!$post->user->created_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>updated_at : </i></b>
                </td>
                <td>{!!$post->user->updated_at!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection