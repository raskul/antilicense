@extends('layouts.layout')
@section('content')

    <section class="content">
        <h1>
            ویرایش
        </h1>
        <a href="{!!url('/')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> صفحه اصلی </a>
        <br>
        {!! Form::model($post, ['route'=>['post.update.cpost', $post->id ], 'method' => 'patch' ]) !!}

        <div class="form-group {{$errors->has('text') ? ' has-error' : ''}} ">
            {!! Form::label('text', 'متن:') !!}
            {!! Form::textarea('text', null, ['class' => 'form-control', 'id'=>'editorcm']) !!}
            @if($errors->has("text"))
                <span class="help-block"><strong> {{$errors->first("text")}} </strong></span>
            @endif
        </div>

        <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> بروز رسانی </button>
        {!! Form::close() !!}

    </section>

@endsection

@push('footer')
    {{--id textarea -> 'id'=> 'editorcm'--}}
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('textarea#editorcm').ckeditor();
        });
    </script>
@endpush