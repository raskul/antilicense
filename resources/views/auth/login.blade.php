@extends('layouts.layout')
@push('header')
    <style>
        html,
        body {
            height: 100%;
            background-image: url(uploads/default/Majestic-Peak-Mount-Cook-New-Zealand.jpg);
            text-shadow: 1px 1px 2px black, 0 0 25px blue, 0 0 5px darkblue;
        }
    </style>
@endpush
@section('content')

<div class="mega-site-preloader-wrap">
    <div class="cont">
        <div class="line square"></div>
        <div class="line square2"></div>
        <div class="line square3"></div>
        <div class="line square4"></div>
    </div>
</div>
<div class="fullwidth-cotnent-block ">
    <div class="fullwidth-content-tablecell">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
                    <div class="login-style-1">
                        <div class="login-logo">
                            <h1><a href="{{ route('index.index') }}">antilicense.com</a></h1>
                        </div>
                        <h2>وارد اکانت خود شوید</h2>
                        <p>امیدواریم تمام نیاز های شما را در زمینه آنتی ویروس مرتفع سازیم</p>
                        <br><br>

                        <div class="login-form-style-1">
                            <form action="{{ route('login') }}" method="post">
                                {{ csrf_field() }}

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                                <p><input style="background-color: #faffbd;color: blue" type="email" placeholder="ایمیل" name="email" value="{{ old('email') }}" required autofocus></p>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                                <p><input style="background-color: #faffbd;color: blue" type="password" placeholder="رمز عبور" name="password" required></p>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> مرا به خاطر بسپار
                                    </label>
                                </div>
                                <p><button type="submit" class="btn btn-primary">ورود</button></p>
                                <br>
                                <br>
                                <div >
                                    <div class=" pull-right" >
                                        <a href="{{ route('password.request') }}" class="btn btn-link">
                                            <span style="background-color: rgb(250,255,189)">
                                            رمز عبور خود را فراموش کرده اید؟
                                            </span>
                                        </a>
                                        <br>
                                        <a href="{{ url('register') }}" class="btn btn-link pull-right">
                                            <span style="background-color: rgb(250,255,189)">
                                             ثبت نام
                                            </span>
                                        </a>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
