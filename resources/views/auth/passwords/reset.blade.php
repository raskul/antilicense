@extends('layouts.layout')
@push('header')
    <style>
        html,
        body {
            height: 100%;
            background-image: url(../uploads/default/Majestic-Peak-Mount-Cook-New-Zealand.jpg);
            text-shadow: 1px 1px 2px black, 0 0 25px blue, 0 0 5px darkblue;
        }
    </style>
@endpush
@section('content')

    <div class="mega-site-preloader-wrap">
        <div class="cont">
            <div class="line square"></div>
            <div class="line square2"></div>
            <div class="line square3"></div>
            <div class="line square4"></div>
        </div>
    </div>
    <div class="fullwidth-cotnent-block ">
        <div class="fullwidth-content-tablecell">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
                        <div class="login-style-1">
                            <div class="login-logo">
                                <h1><a href="{{ route('index.index') }}">antilicense.com</a></h1>
                            </div>
                            <h2>ریست کردن پسورد</h2>
                            <p>امیدواریم تمام نیاز های شما را در زمینه آنتی ویروس مرتفع سازیم</p>
                            <br><br>

                            <div class="login-form-style-1">
                                <form action="{{ route('password.request') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="token" value="{{ $token }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                    <p><input style="background-color: #faffbd;color: blue" type="email" placeholder="ایمیل" name="email" value="{{ $email or old('email') }}" required autofocus></p>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <p><input style="background-color: #faffbd;color: blue" type="password" placeholder="رمز عبور" name="password" required></p>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <p><input style="background-color: #faffbd;color: blue" type="password" placeholder="تکرار رمز عبور" name="password_confirmation" required></p>


                                    <p><button type="submit" class="btn btn-primary">ریست کردن پسورد</button></p>
                                    <br>
                                    <br>
                                    <div >
                                        <div class=" pull-right" >
                                            <a href="{{ url('login') }}" class="btn btn-link pull-right">
                                            <span style="background-color: rgb(250,255,189)">
                                            ورود
                                            </span>
                                            </a>
                                            <br>
                                            <a href="{{ url('register') }}" class="btn btn-link pull-right">
                                            <span style="background-color: rgb(250,255,189)">
                                             ثبت نام
                                            </span>
                                            </a>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection