@extends('layouts.layout')
@push('header')
    <style>
        html,
        body {
            height: 100%;
            background-image: url(../uploads/default/Majestic-Peak-Mount-Cook-New-Zealand.jpg);
            text-shadow: 1px 1px 2px black, 0 0 25px blue, 0 0 5px darkblue;
        }
    </style>
@endpush
@section('content')

    <div class="mega-site-preloader-wrap">
        <div class="cont">
            <div class="line square"></div>
            <div class="line square2"></div>
            <div class="line square3"></div>
            <div class="line square4"></div>
        </div>
    </div>
    <div class="fullwidth-cotnent-block ">
        <div class="fullwidth-content-tablecell">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
                        <div class="login-style-1">
                            <div class="login-logo">
                                <h1><a href="{{ route('index.index') }}">antilicense.com</a></h1>
                            </div>
                            <h2>ریست کردن پسورد</h2>
                            <p>امیدواریم تمام نیاز های شما را در زمینه آنتی ویروس مرتفع سازیم</p>
                            <br><br>

                            <div class="login-form-style-1">
                                <form action="{{ route('password.email') }}" method="post">
                                    {{ csrf_field() }}

                                    @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    @endif

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

                                    <p><input style="background-color: #faffbd;color: blue" type="email" placeholder="ایمیل" name="email" value="{{ old('email') }}" required autofocus></p>

                                    <p><button type="submit" class="btn btn-primary">فرستادن لینک ریست کردن پسورد</button></p>
                                    <br>
                                    <br>
                                    <div >
                                        <div class=" pull-right" >
                                            <a href="{{ url('login') }}" class="btn btn-link pull-right">
                                            <span style="background-color: rgb(250,255,189)">
                                            ورود
                                            </span>
                                            </a>
                                            <br>
                                            <a href="{{ url('register') }}" class="btn btn-link pull-right">
                                            <span style="background-color: rgb(250,255,189)">
                                             ثبت نام
                                            </span>
                                            </a>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection