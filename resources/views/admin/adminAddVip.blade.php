@extends('scaffold-interface.layouts.app')
@section('title','Create')
@push('header')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endpush
@section('content')
    <section class="content">
        <h1>
            افزودن vip توسط ادمین
        </h1>
        <a href="{!!route('adminAddVip')!!}" class='btn btn-danger'><i class="fa fa-home"></i> صفحه اصلی </a>
        <br>
        {!! Form::open(['route'=>['adminAddVip' ], 'method' => 'POST' ]) !!}

        <div class="form-group">
            <label>انتخاب یوزر</label>
            <select name='user' class='form-control addselect'>
                @foreach($users as $user)
                    <option value="{{$user->id}}">{{$user->email}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>تعداد ماه</label>
            <select name="month" class="form-control addselect">
                @for($i = 1; $i <= 15; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                @endfor
            </select>
        </div>

        <button class='btn btn-success' type='submit'><i class="fa fa-floppy-o"></i> ذخیره </button>

        {!! Form::close() !!}
    </section>
@endsection
@push('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.addselect').select2();
        });
    </script>
@endpush