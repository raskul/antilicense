<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="لایسنس آنتی ویروس">
    <meta name="author" content="Sadegh">
    <title>@yield('title')</title>
    <title>لایسنس آنتی ویروس</title>

    <!-- Bootstrap -->
    <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('iransans/css/fontiran.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">


    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{asset('uploads/images/favicon.png')}}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">

    <!-- OwlCarousel CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">

    <!-- Magnific popup CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}">

    <!-- Slicknav CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/slicknav.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/perfect-scrollbar.min.css')}}">

    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

    @stack('header')

    <style>
        body {
            font-family: IRANSans !important;
            font-weight: 300;
            direction: rtl;
            margin: 0;
        }
        h1, h2, h3, h4, h5, h6,input, textarea {
            font-family: IRANSans !important;
        }
        .datepicker-plot-area {
            position: relative;
        }
        .tooltip{
            font-family: IRANSans, Tahoma;
        }
    </style>

</head>
<body>
<div class="container-fluid">
    <div class="row ">

        @yield('content')

    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>


<script>$(window).load(function() {
        $("img").removeClass("preload");
    });</script>


<!-- Magnific Popup JS -->
<script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>

<!-- OwlCarousel JS -->
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>

<!-- Parallax JS -->
<script src="{{asset('assets/js/parallax.min.js')}}"></script>

<!-- WOW JS -->
<script src="{{asset('assets/js/wow-1.3.0.min.js')}}"></script>

<!-- SlickNav JS -->
<script src="{{asset('assets/js/jquery.slicknav.min.js')}}"></script>

<!-- PerfectScrollbar JS -->
<script src="{{asset('assets/js/jquery-perfect-scrollbar.min.js')}}"></script>

<!-- Active JS -->
<script src="{{asset('assets/js/active.js')}}"></script>

@stack('footer')
</body>
</html>
