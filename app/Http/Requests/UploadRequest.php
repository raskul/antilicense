<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file1' => 'mimes:jpg,jpeg,zip|max:2048',
            'file2' => 'mimes:jpg,jpeg,zip|max:2048',
            'file3' => 'mimes:jpg,jpeg,zip|max:2048',
            'file4' => 'mimes:jpg,jpeg,zip|max:2048',
            'file5' => 'mimes:jpg,jpeg,zip|max:2048'
        ];
    }
}
