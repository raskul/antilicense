<?php

namespace App\Http\Middleware;

use App\Log;
use Auth;
use Closure;

class VisitorsLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $call_back_url = '';
        ///////////////////ip:
        $ip = $_SERVER['REMOTE_ADDR'];
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        ///////////////////////server information:
        $server = '';
        $server .= ' $_SERVER[REMOTE_ADDR]: ';
        if (isset($_SERVER['REMOTE_ADDR']))
            $server .= $_SERVER['REMOTE_ADDR'];//"127.0.0.1"
        $server .= ' completeURL: ';
        if (isset($_SERVER['HTTP_HOST']) && isset($_SERVER['PHP_SELF']))
            $server .= "http://" . $_SERVER['HTTP_HOST'] . "" . $_SERVER['PHP_SELF'];//"http://antilicense.dev/index.php" //url feli
        if (isset($_SERVER['HTTP_REFERER']))
            $call_back_url = $_SERVER['HTTP_REFERER'];//"http://antilicense.dev/" //url yad avar
//        $server .= '  ';
//        $server .= $_SERVER['HTTP_USER_AGENT'];//"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36"
//        $server .= ' country: ';
//        $array = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
//"geoplugin_countryCode" => "IR"
//"geoplugin_countryName" => "Iran, Islamic Republic of"
//        $server .= $array['geoplugin_countryCode'];

//        dd($_SERVER['HTTP_CLIENT_IP']);

        ///////////////os :
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        /////////////////browser:
        if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/OPR/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        if (Auth::check()) {
            $username = auth()->user()->name;
        }else{
            $username = null;
        }
        ///////////////////
//        $brower = $ub;
//        $ip = $ip;
//        $platform = $platform;
//        $server = $server;
//        $brower = $brower;
        ///////////////////
        Log::create(['ip' => $ip, 'url' => $server, 'call_back_url' => $call_back_url, 'os' => $platform, 'browser' => $ub, 'username' => $username]);

        return $next($request);
    }
}
