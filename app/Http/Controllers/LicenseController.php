<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\License;
use Amranidev\Ajaxis\Ajaxis;
use URL;

use App\Antivirus;


/**
 * Class LicenseController.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:24:34pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class LicenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('is-editor');

        $title = 'Index - license';
        $licenses = License::orderBy('updated_at', 'desc')->paginate(50);
        return view('license.index',compact('licenses','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('is-editor');

        $title = 'Create - license';
        
        $antiviruses = Antivirus::all()->pluck('name','id');
        
        return view('license.create',compact('title','antiviruses'  ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('is-editor');

        $license = new License();
        
        $license->content_one = $request->content_one;
        $license->content_one_vip = $request->content_one_vip;
        $license->content_two = $request->content_two;
        $license->content_two_vip = $request->content_two_vip;
        $license->content_three = $request->content_three;
        $license->content_three_vip = $request->content_three_vip;
        $license->content_four = $request->content_four;
        $license->content_four_vip = $request->content_four_vip;
        $license->content_five = $request->content_five;
        $license->modify_user_id = auth()->user()->id;
        $license->antivirus_id = $request->antivirus_id;

        $license->save();

//        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
//        $pusher->trigger('test-channel',
//                         'test-event',
//                        ['message' => 'A new license has been created !!']);

        return redirect('license');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $this->authorize('is-editor');

        $title = 'Show - license';

        if($request->ajax())
        {
            return URL::to('license/'.$id);
        }

        $license = License::findOrfail($id);
        return view('license.show',compact('title','license'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $this->authorize('is-editor');

        $title = 'Edit - license';
        if($request->ajax())
        {
            return URL::to('license/'. $id . '/edit');
        }

        
        $antiviruses = Antivirus::all()->pluck('name','id');

        $license = License::findOrfail($id);

        return view('license.edit',compact('title','license' ,'antiviruses' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $this->authorize('is-editor');

        $license = License::findOrfail($id);
        $license->content_one = $request->content_one;
        $license->content_one_vip = $request->content_one_vip;
        $license->content_two = $request->content_two;
        $license->content_two_vip = $request->content_two_vip;
        $license->content_three = $request->content_three;
        $license->content_three_vip = $request->content_three_vip;
        $license->content_four = $request->content_four;
        $license->content_four_vip = $request->content_four_vip;
        $license->content_five = $request->content_five;
        $license->modify_user_id = auth()->user()->id;
        $license->antivirus_id = $request->antivirus_id;
        
        $license->save();

        return redirect('license');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::BtDeleting('هشدار','آیا اطمینان دارید که میخواهید پاک شود؟','/license/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('is-editor');

     	$license = License::findOrfail($id);
     	$license->delete();
        return URL::to('license');
    }
}
