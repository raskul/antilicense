<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function adminAddVip(Request $request)
    {
        $this->authorize('is-admin');

        if ($request->isMethod('POST')){
            $user = User::find($request->user);
            $time = ($request->month * 30 * 24 * 60 * 60) + time();
            $time = date('Y-m-d H:i:s', $time);
            $user->update(['vip_end_date' => $time, 'role_id' => 2]);
            return redirect()->back();
        }
        $users = User::get();

        return view('admin.adminAddVip', compact('users'));
    }
}
