<?php

namespace App\Http\Controllers;

use App\Http\Requests\KarbarRequest;
use App\Role;
use App\User;
use DB;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class KarbarController extends Controller
{
    public $path = 'uploads/images/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('is-admin');

        $users = \App\User::orderBy('updated_at', 'desc')->paginate(100);

        return view('karbar.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('is-admin');

        $role = DB::table('roles')->pluck('name', 'id');

        return view('karbar.create', compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('is-admin');

        $user = new \App\User();

        $user->email = $request->email;
        $user->name = $request->name;

        if (!is_null($request->password))
            $user->password = Hash::make($request->password);

        if (is_null($request->file('image'))) {
            $user->image = 'AnonymousMale.jpg';
        } else {
            $picName = $request->file('image')->getClientOriginalName();
            $request->image->move($this->path, $picName);
            $user->image = $picName;
        }

        if (!is_null($request->role_id))
            $user->role_id = $request->role_id;

        if (!is_null($request->about))
            $user->about = $request->about;

        $user->save();

        return redirect(route('karbar.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $karbar = User::where('id', $id)->first();

        return view('karbar.show', compact('karbar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('is-profile-owner', $id);

        $user = User::where('id', $id)->first();
        $role = Role::pluck('name', 'id');

        return view('karbar.edit', compact('user', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, KarbarRequest $request)
    {

//        $input = new User();
//        $input->email = $request->email;
//        $input->name = $request->name;
//        $input->password = Hash::make($request->password);
//        if (!$input->image)
//            $input->image = 'AnonymousMale.jpg';
//        $input->role_id = $request->role_id;
//        $input->id = $id;
////        $array = (array)$input;
////        return ($array);
//        $input->update();
//        User::where('id', $id)->update($input);

        $this->authorize('is-profile-owner', $id);

//        $input['email'] = $request->email;

        $input['name'] = $request->name;
        $cond = User::where('name', $input['name'])->where('id', '!=', $id)->first();
        if ($cond)
            return message('نام شما قبلا توسط شخص دیگری گرفته شده است.لطفا یک نام دیگر انتخواب نمایید');

        $input['password'] = Hash::make($request->password);

        if (!is_null($request->file('image'))) {
            $picName = $request->file('image')->getClientOriginalName();
            $request->image->move($this->path, $picName);
            $input['image'] = $picName;
        }

        $input['about'] = $request->about;

        if (Gate::allows('is-admin'))
            $input['role_id'] = $request->role_id;

        User::where('id', $id)->update($input);

        if (Gate::allows('is-admin'))
            return redirect(route('karbar.index'));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('is-admin');

        $user = User::find($id);

        $user->delete();

        return redirect(route('karbar.index'));
    }
}
