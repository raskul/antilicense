<?php

namespace App\Http\Controllers;

use App\User;
use App\Zarin;
use Illuminate\Support\Facades\Input;
use Zarinpal\Zarinpal;

class ZarinController extends Controller
{

    public function create()
    {
        return view('index.vip');
    }

    public function planMonthly(Zarinpal $zarinpal)
    {
        $payment = [
            'CallbackURL' => route('payment.verify.monthly'), // Required
            'Amount'      => 1000,                                  // Required
            'Description' => 'buy monthly plan',                    // Required
            'Email'       => auth()->user()->email,                 // Optional
            'Mobile'      => '09331234567'                          // Optional
        ];
        $response = $zarinpal->request($payment);
        if ($response['Status'] === 100) {
            $authority = $response['Authority'];

            return $zarinpal->redirect($authority);
        }

        return 'Error,
    Status: ' . $response['Status'] . ',
    Message: ' . $response['Message'];
    }

    public function planYearly(Zarinpal $zarinpal)
    {
        $payment = [
            'CallbackURL' => route('payment.verify.yearly'),  // Required
            'Amount'      => 5000,                                  // Required
            'Description' => 'buy yearly plan',                     // Required
            'Email'       => auth()->user()->email,                 // Optional
            'Mobile'      => '09331234568'                          // Optional
        ];
        $response = $zarinpal->request($payment);
        if ($response['Status'] === 100) {
            $authority = $response['Authority'];

            return $zarinpal->redirect($authority);
        }

        return 'Error,
    Status: ' . $response['Status'] . ',
    Message: ' . $response['Message'];
    }

    public function verifyMontyly(Zarinpal $zarinpal)
    {
        $payment = [
            'Authority' => Input::get('Authority'), // $_GET['Authority']
            'Status'    => Input::get('Status'),    // $_GET['Status']
            'Amount'    => 1000,
        ];
        $response = $zarinpal->verify($payment);
        if ($response['Status'] === 100) {
            $this->afterVerify($response, $payment, $status = 'monthly ok');

            return message(
                $response['Message']
                .
                "<br>"
                .
                "شماره پیگیری:"
                .
                $response['RefID']
                , 'index.index', 'بازگشت به صفحه اصلی سایت', 'پیغام سیستم', 'success');
        }

        return 'Error,
    Status: ' . $response['Status'] . ',
    Message: ' . $response['Message'];
    }

    public function verifyYearly(Zarinpal $zarinpal)
    {
        $payment = [
            'Authority' => Input::get('Authority'), // $_GET['Authority']
            'Status'    => Input::get('Status'),    // $_GET['Status']
            'Amount'    => 5000,
        ];
        $response = $zarinpal->verify($payment);
        if ($response['Status'] === 100) {
            $this->afterVerify($response, $payment, $status = 'yearly ok');

            return message(
                $response['Message']
                .
                '<br>'
                .
                "شماره پیگیری:"
                .
                $response['RefID']
                , 'index.index', 'بازگشت به صفحه اصلی سایت', 'پیغام سیستم', 'success');
        }

        return 'Error,
    Status: ' . $response['Status'] . ',
    Message: ' . $response['Message'];
    }

    public function afterVerify($response, $payment, $status)
    {
        Zarin::create([
            'ref_id'   => $response['RefID'],
            'message'  => $response['Message'],
            'username' => auth()->user()->name,
            'amount'   => $payment['Amount'],
            'email'    => auth()->user()->email,
            'status'   => $status,
        ]);

        if ((auth()->user()->role_id == 1) || (auth()->user()->role_id == 2))
            User::where('id', auth()->user()->id)->update(['role_id' => 2]);

        if ((auth()->user()->vip_end_date == null) || ((strtotime(auth()->user()->vip_end_date)) < time()))
            User::where('id', auth()->user()->id)->update(['vip_end_date' => strftime('%Y-%m-%d %H:%M:%S', time())]);

        $vipEndTime = strtotime(User::where('id', auth()->user()->id)->pluck('vip_end_date')->first());

        if ($status == 'monthly ok')
            $vipEndTime = $vipEndTime + (60 * 60 * 24 * 30);

        if ($status == 'yearly ok')
            $vipEndTime = $vipEndTime + (60 * 60 * 24 * 365);


        User::where('id', auth()->user()->id)->update(['vip_end_date' => strftime('%Y-%m-%d %H:%M:%S', $vipEndTime)]);

    }

}
