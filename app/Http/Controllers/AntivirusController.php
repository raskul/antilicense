<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Antivirus;
use Amranidev\Ajaxis\Ajaxis;
use URL;
/**
 * Class AntivirusController.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:06:36pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class AntivirusController extends Controller
{

    public $path = 'uploads/images/';
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('is-admin');

        $title = 'Index - antivirus';
        $antiviruses = Antivirus::orderBy('updated_at', 'desc')->paginate(50);
        return view('antivirus.index',compact('antiviruses','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('is-admin');

        $title = 'Create - antivirus';

        return view('antivirus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('is-admin');

        $antivirus = new Antivirus();

        $picName = $request->file('image')->getClientOriginalName();
        $request->image->move($this->path, $picName);

        $antivirus->image = $picName;
        $antivirus->name = $request->name;
        $antivirus->name_fa = $request->name_fa;

        $antivirus->save();

//        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
//        $pusher->trigger('test-channel',
//                         'test-event',
//                        ['message' => 'A new antivirus has been created !!']);

        return redirect('antivirus');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show(Antivirus $antivirus)
    {

        $title = 'Show - antivirus';

        $downloadPost = $antivirus->posts()->where('type_id', 4 )->first();
        $learnPosts = $antivirus->posts()->where('type_id', 1 )->get();
        $aboutPost = $antivirus->posts()->where('type_id', 5)->first();
        $comparePost = $antivirus->posts()->where('type_id', 7)->first();
        $noghatZafGhodratPost = $antivirus->posts()->where('type_id', 6)->first();

        $license['one'] = $antivirus->license()->pluck('content_one')->first();
        $license['one_vip'] = $antivirus->license()->pluck('content_one_vip')->first();
        $license['two'] = $antivirus->license()->pluck('content_two')->first();
        $license['two_vip'] = $antivirus->license()->pluck('content_two_vip')->first();
        $license['three'] = $antivirus->license()->pluck('content_three')->first();
        $license['three_vip'] = $antivirus->license()->pluck('content_three_vip')->first();
        $license['four'] = $antivirus->license()->pluck('content_four')->first();
        $license['four_vip'] = $antivirus->license()->pluck('content_four_vip')->first();
        $license['five'] = $antivirus->license()->pluck('content_five')->first();

        $cPosts = $antivirus->posts()->where('type_id', 2)->orWhere('type_id', 3)->get();
        $cStarPosts = $antivirus->posts()->where('type_id', 3)->get();

        return view('index.second',compact('title','antivirus','downloadPost', 'learnPosts', 'aboutPost', 'comparePost', 'noghatZafGhodratPost', 'license', 'cPosts', 'cStarPosts'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('is-admin');

        $title = 'Edit - antivirus';
        
        $antivirus = Antivirus::findOrfail($id);
        return view('antivirus.edit',compact('title','antivirus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $this->authorize('is-admin');

        $antivirus = Antivirus::findOrfail($id);
    	
        $antivirus->name = $request->name;

        $antivirus->name_fa = $request->name_fa;

        $picName = $request->file('image')->getClientOriginalName();
        $request->image->move($this->path, $picName);

        $antivirus->image = $picName;

        $antivirus->save();

        return redirect('antivirus');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::BtDeleting('هشدار','آیا اطمینان دارید که میخواهید پاک شود؟','/antivirus/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('is-admin');

     	$antivirus = Antivirus::findOrfail($id);
     	$antivirus->delete();
        return redirect()->back();
    }
}
