<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class TagController.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:26:08pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('is-writer');

        $title = 'Index - tag';
        $tags = Tag::orderBy('updated_at', 'desc')->paginate(50);
        return view('tag.index',compact('tags','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('is-writer');

        $title = 'Create - tag';
        
        return view('tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('is-writer');

        $tag = new Tag();

        
        $tag->name = $request->name;

        
        
        $tag->save();

//        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
//        $pusher->trigger('test-channel',
//                         'test-event',
//                        ['message' => 'A new tag has been created !!']);

        return redirect('tag');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $this->authorize('is-writer');

        $title = 'Show - tag';

        if($request->ajax())
        {
            return URL::to('tag/'.$id);
        }

        $tag = Tag::findOrfail($id);
        return view('tag.show',compact('title','tag'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $this->authorize('is-editor');

        $title = 'Edit - tag';
        if($request->ajax())
        {
            return URL::to('tag/'. $id . '/edit');
        }

        
        $tag = Tag::findOrfail($id);
        return view('tag.edit',compact('title','tag'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $this->authorize('is-editor');

        $tag = Tag::findOrfail($id);
    	
        $tag->name = $request->name;
        
        
        $tag->save();

        return redirect('tag');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $this->authorize('is-editor');

        $msg = Ajaxis::BtDeleting('هشدار','آیا اطمینان دارید که میخواهید پاک شود؟','/tag/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$tag = Tag::findOrfail($id);
     	$tag->delete();
        return URL::to('tag');
    }
}
