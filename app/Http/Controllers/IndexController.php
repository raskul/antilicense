<?php

namespace App\Http\Controllers;

use App\Antivirus;
use App\Http\Requests\UploadRequest;
use App\License;
use App\Post;
use App\User;

class IndexController extends Controller
{
    public $path = 'uploads/files/';


    public function home()
    {
        //list of antivirus in index page
        $antivirus = Antivirus::where('is_active', 1)->orderBy('order')->get();

        //posts down of index page:
        $posts = Post::orderBy('updated_at', 'desc')->paginate(18);

        return view('index.index', compact('antivirus', 'posts'));
    }

    public function license($id)
    {
        $title = 'لایسنس';
        $antivirus = Antivirus::find($id);
        $text = License::where('antivirus_id', $id)->first();

        return view('index.second', compact('title', 'antivirus', 'text'));
    }

    public function download($id)
    {
        $title = 'دانلود جدیدترین ورژن';
        $antivirus = Antivirus::find($id);
        $text = Post::where('antivirus_id', $id)->where('type_id', 4)->first();

        return view('index.second', compact('title', 'antivirus', 'text'));
    }

    public function learn($id)
    {
        $title = 'آموزش';
        $antivirus = Antivirus::find($id);
        $text = Post::where('antivirus_id', $id)->where('type_id', 1)->first();

        return view('index.second', compact('title', 'antivirus', 'text'));
    }

    public function comments($id)
    {
        $title = 'دیدگاه های';
        $antivirus = Antivirus::find($id);
        $text = Post::where('antivirus_id', $id)->where('type_id', 2)->orWhere('type_id', 3)->get();

        return view('index.second', compact('title', 'antivirus', 'text'));
    }

    public function starComments($id)
    {
        $title = 'دیدگاه های منتخب';
        $antivirus = Antivirus::find($id);
        $text = Post::where('antivirus_id', $id)->where('type_id', 3)->get();

        return view('index.second', compact('title', 'antivirus', 'text'));
    }

    public function aboutAntivirus($id)
    {
        $title = 'درباره';
        $antivirus = Antivirus::find($id);
        $text = Post::where('antivirus_id', $id)->where('type_id', 5)->first();

        return view('index.second', compact('title', 'antivirus', 'text'));
    }

    public function noghatGhovat($id)
    {
        $title = 'نقاط قوت و ضعف';
        $antivirus = Antivirus::find($id);
        $text = Post::where('antivirus_id', $id)->where('type_id', 6)->first();

        return view('index.second', compact('title', 'antivirus', 'text'));
    }

    public function compareAntivirus($id)
    {
        $title = 'مقایسه';
        $antivirus = Antivirus::find($id);
        $text = Post::where('antivirus_id', $id)->where('type_id', 7)->first();

        return view('index.second', compact('title', 'antivirus', 'text'));
    }

//    //////////////////////////////////////////////

    public function post($id)
    {
        $post = Post::find($id);

        return view('index.post', compact('post'));
    }

    public function uploadGet()
    {
        return view('index.upload');
    }

    public function uploadPost(UploadRequest $request)
    {
        $this->authorize('is-writer');

        $username = auth()->user()->name;
        $fullPath = $this->path . $username . '/';
        $time = time();

        if (isset($request->file1)) {
            $inputs['name1'] = $time . $request->file('file1')->getClientOriginalName();
            $request->file1->move($fullPath, $inputs['name1']);
            $url1 = route('index.index') . '/' . $fullPath . $inputs['name1'];
        }
        if (isset($request->file2)) {
            $inputs['name2'] = $time . $request->file('file2')->getClientOriginalName();
            $request->file2->move($fullPath, $inputs['name2']);
            $url2 = route('index.index') . '/' . $fullPath . $inputs['name2'];
        }
        if (isset($request->file3)) {
            $inputs['name3'] = $time . $request->file('file3')->getClientOriginalName();
            $request->file3->move($fullPath, $inputs['name3']);
            $url3 = route('index.index') . '/' . $fullPath . $inputs['name3'];
        }
        if (isset($request->file4)) {
            $inputs['name4'] = $time . $request->file('file4')->getClientOriginalName();
            $request->file4->move($fullPath, $inputs['name4']);
            $url4 = route('index.index') . '/' . $fullPath . $inputs['name4'];
        }
        if (isset($request->file5)) {
            $inputs['name5'] = $time . $request->file('file5')->getClientOriginalName();
            $request->file5->move($fullPath, $inputs['name5']);
            $url5 = route('index.index') . '/' . $fullPath . $inputs['name5'];
        }

        return view('index.upload', compact('url1', 'url2', 'url3', 'url4', 'url5'));

    }

    public function dashboard()
    {
        $users = User::get()->count();

        return view('scaffold-interface.dashboard.dashboard', compact('users'));
    }

    public function compareAll()
    {
        return view('index.compareAll');
    }

    public function bestAntivirus()
    {
        return view('index.bestAntivirus');
    }
}
