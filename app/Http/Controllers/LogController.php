<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Log;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class LogController.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:31:34pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('is-admin');

        $title = 'Index - log';
        $logs = Log::orderBy('updated_at', 'desc')->paginate(50);
        return view('log.index',compact('logs','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('is-admin');

        $title = 'Create - log';
        
        return view('log.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('is-admin');

        $log = new Log();
        $log->ip = $request->ip;
        $log->url = $request->url;
        $log->call_back_url = $request->call_back_url;
        $log->os = $request->os;
        $log->browser = $request->browser;

        $log->save();

//        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
//        $pusher->trigger('test-channel',
//                         'test-event',
//                        ['message' => 'A new log has been created !!']);

        return redirect('log');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $this->authorize('is-admin');

        $title = 'Show - log';

        if($request->ajax())
        {
            return URL::to('log/'.$id);
        }

        $log = Log::findOrfail($id);
        return view('log.show',compact('title','log'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $this->authorize('is-admin');

        $title = 'Edit - log';
        if($request->ajax())
        {
            return URL::to('log/'. $id . '/edit');
        }

        
        $log = Log::findOrfail($id);
        return view('log.edit',compact('title','log'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $this->authorize('is-admin');

        $log = Log::findOrfail($id);
        $log->ip = $request->ip;
        $log->url = $request->url;
        $log->call_back_url = $request->call_back_url;
        $log->os = $request->os;
        $log->browser = $request->browser;
        
        $log->save();

        return redirect('log');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::BtDeleting('هشدار','آیا اطمینان دارید که میخواهید پاک شود؟','/log/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('is-admin');

     	$log = Log::findOrfail($id);
     	$log->delete();
        return URL::to('log');
    }
}
