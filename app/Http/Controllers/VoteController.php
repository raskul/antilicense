<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vote;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class VoteController.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:28:06pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class VoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('is-admin');

        $title = 'Index - vote';
        $votes = Vote::orderBy('updated_at', 'desc')->paginate(50);
        return view('vote.index',compact('votes','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('is-admin');

        $title = 'Create - vote';
        
        return view('vote.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('is-admin');

        $vote = new Vote();
        $vote->type = $request->type;
        $vote->user_id = $request->user_id;
        $vote->post_id = $request->post_id;
        $vote->save();

//        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
//        $pusher->trigger('test-channel',
//                         'test-event',
//                        ['message' => 'A new vote has been created !!']);

        return redirect('vote');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - vote';

        if($request->ajax())
        {
            return URL::to('vote/'.$id);
        }

        $vote = Vote::findOrfail($id);
        return view('vote.show',compact('title','vote'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $this->authorize('is-admin');

        $title = 'Edit - vote';
        if($request->ajax())
        {
            return URL::to('vote/'. $id . '/edit');
        }

        $vote = Vote::findOrfail($id);
        return view('vote.edit',compact('title','vote'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $this->authorize('is-admin');

        $vote = Vote::findOrfail($id);
        $vote->type = $request->type;
        $vote->user_id = $request->user_id;
        $vote->post_id = $request->post_id;
        $vote->save();

        return redirect('vote');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::BtDeleting('هشدار','آیا اطمینان دارید که میخواهید پاک شود؟','/vote/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('is-admin');

     	$vote = Vote::findOrfail($id);
     	$vote->delete();
        return URL::to('vote');
    }
}
