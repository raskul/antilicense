<?php

namespace App\Http\Controllers;

use Amranidev\Ajaxis\Ajaxis;
use App\Comment;
use App\Post;
use App\User;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use URL;


/**
 * Class CommentController.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:21:30pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('is-editor');

        $title = 'Index - comment';
        $comments = Comment::orderBy('updated_at', 'desc')->paginate(50);

        return view('comment.index', compact('comments', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('is-editor');

        $title = 'Create - comment';

        $posts = Post::all()->pluck('title', 'id');

        $users = User::all()->pluck('name', 'id');

        return view('comment.create', compact('title', 'posts', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {

            $comment = new Comment();

            $comment->text = $request->text;

            $comment->post_id = $request->post_id;

            $comment->user_id = auth()->user()->id;

            $comment->save();

//            $pusher = App::make('pusher');

            //default pusher notification.
            //by default channel=test-channel,event=test-event
            //Here is a pusher notification example when you create a new resource in storage.
            //you can modify anything you want or use it wherever.
//            $pusher->trigger('test-channel',
//                'test-event',
//                ['message' => 'A new comment has been created !!']);

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request $request
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $this->authorize('is-editor');

        $title = 'Show - comment';

        if ($request->ajax()) {
            return URL::to('comment/' . $id);
        }

        $comment = Comment::findOrfail($id);

        return view('comment.show', compact('title', 'comment'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request $request
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $title = 'Edit - comment';
        if ($request->ajax()) {
            return URL::to('comment/' . $id . '/edit');
        }

        $posts = Post::all()->pluck('title', 'id');

        $users = User::all()->pluck('name', 'id');

        $comment = Comment::findOrfail($id);

        $this->authorize('is-comment-owner', $comment);

        if (Gate::allows('is-admin'))
            return view('comment.edit', compact('title', 'comment', 'posts', 'users'));

        if (Gate::allows('is-comment-owner', $comment) || Gate::allows('is-editor'))
            return view('comment.editLite', compact('title', 'comment'));

        $this->authorize('is-editor');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $comment = Comment::findOrfail($id);

        $this->authorize('is-admin', $comment);

        $comment->text = $request->text;

        $comment->post_id = $request->post_id;

        $comment->user_id = $request->user_id;

        $comment->save();

        return redirect('comment');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request $request
     * @return  String
     */
    public function DeleteMsg($id, Request $request)
    {
        $msg = Ajaxis::BtDeleting('هشدار', 'آیا اطمینان دارید که میخواهید پاک شود؟', '/comment/' . $id . '/delete');

        if ($request->ajax()) {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('is-editor');

        $comment = Comment::findOrfail($id);
        $comment->delete();

        return URL::to('comment');
    }

    public function liteUpdate($id, Request $request)
    {
        $comment = Comment::find($id);
        $comment->text = $request->text;

        if (Gate::allows('is-comment-owner', $comment) || Gate::allows('is-editor'))
            $comment->save();

        return redirect()->back();
    }
}
