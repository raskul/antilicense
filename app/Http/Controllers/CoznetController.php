<?php

namespace App\Http\Controllers;

use App\Antivirus;
use App\CozneList;
use App\CoznePost;
use App\License;
use Sunra\PhpSimple\HtmlDomParser;

class CoznetController extends Controller
{
    public function create($id)
    {
        if (isset($id)) {
            $lists = CozneList::where('antivirus_id', $id)->pluck('name');
        } else {
            $lists = CozneList::pluck('name');
        }


        for ($z = 2; $z >= 1; $z--) {
            //links of page:
//        $html = file_get_contents('2.html');
//        $html = file_get_contents('http://tais-afinskaja.ucoz.net/news/?page1');//http://tais-afinskaja.ucoz.net/news/?page2

            $html = file_get_contents("http://tais-afinskaja.ucoz.net/news/?page$z");
            $html = HtmlDomParser::str_get_html($html)->find('body')[0];

            //find all links and titles in page :
            $links = [];
            $title = [];
            for ($i = 0; $i <= 9; $i++) {
                $links[$i] = 'http://tais-afinskaja.ucoz.net' . $html->find('.eTitle a', $i)->href;
                $title[$i] = $html->find('.eTitle', $i)->find('a', 0)->outertext();
            }


            //go to links that we found
            for ($i = 0; $i <= 9; $i++) {
                $this->get_post_content($title[$i], $links[$i], $lists);
            }

        }
    }

    /**
     * @param $title of posts in url
     * @param $links of posts in url
     * @param $list of conzlist
     * @return all post content and store usernames to database coznepost
     */
    public function get_post_content($title, $links, $lists)
    {
        $postHtml = file_get_contents($links);

        foreach ($lists as $list) {
            //date of the post:
            $clock = HtmlDomParser::str_get_html($postHtml)->find('.eTitle', 0)->find('div', 0)->innertext();
            preg_match('/\b[0-9]{4}-[0-9]{2}-[0-9]{2}\b/', $links, $temp);
            $postDate = strtotime($temp[0] . ' ' . $clock);

            //title of the post : $title[$i]
            if (stripos($title, $list)) {
                $antiviruses = Antivirus::get();
                foreach ($antiviruses as $antivirus) {
                    if (stripos(' ' . $list, strtr($antivirus->name, [' ' => '', '-' => '_']))) {
                        $antiviruId = $antivirus->id;

                        $postContent = HtmlDomParser::str_get_html($postHtml)->find('.eMessage', 0);


                        $list = strtolower($list);
                        $func = strtr($list, [' ' => '_', '.' => '_', '-' => '_']);
                        $result = $this->$func($postContent);

                        if ($result) {
                            $username = $result[0];

                            if (strlen($username) < 8)
                                $username = 'error';

                            $type = $result[1];
                            CoznePost::create([
                                'antivirus_id' => $antiviruId,
                                'type'         => $type,
                                'username'     => $username,
                                'link'         => $links,
                                'pub_date'     => $postDate,
                                'title'        => $title,
                            ]);

                            if (strlen($username) >= 8) {
                                if (stripos($type, 'internet')) {
                                    License::where('antivirus_id', $antiviruId)->update([
                                        'content_three'      => $type,
                                        'content_three_vip'  => $username,
                                        'original_post_date' => $postDate,
                                        'modify_user_id'     => 'MeGa',
                                    ]);
                                } else {
                                    License::where('antivirus_id', $antiviruId)->update([
                                        'content_two'        => $type,
                                        'content_two_vip'    => $username,
                                        'original_post_date' => $postDate,
                                        'modify_user_id'     => 'MeGa',
                                    ]);
                                }
                            }
                        }
                    }
                }
                if (isset($username)){
                    echo $type . '<br>';
                    echo $username . '<br>' . '<br>';
                }
            }
        }
    }

    /**
     * @param $content
     * @return string
     * translate to english language
     */
    public function translateToEnglish($content)
    {
        $array = [
            ': '          => ' : ',
            ' :'          => ' : ',
            'Nome utente' => 'username',//ru to en
            'Contrasena'    => 'password',
            'expiredate'  => 'expiredate',
        ];

        return strtr($content, $array);
    }


    public function kaspersky_total($content)
    {
        if (stripos($content, 'india'))
            return null;
        if (preg_match_all('/\b[0-z]{5}-[0-z]{5}-[0-z]{5}-[0-z]{5}\b/', $content, $username)) {
            $result[0] = implode('<br>', $username[0]);
            $result[0] = $this->translateToEnglish($result[0]);
            $result[1] = 'kaspersky_total_security';

            return $result;
        }

        return null;
    }


    public function kaspersky_internet($content)
    {
        if (stripos($content, 'india'))
            return null;
        if (preg_match_all('/\b[0-z]{5}-[0-z]{5}-[0-z]{5}-[0-z]{5}\b/', $content, $username)) {
            $result[0] = implode('<br>', $username[0]);
            $result[0] = $this->translateToEnglish($result[0]);
            $result[1] = 'Kaspersky Internet Security';

            return $result;
        }

        return null;
    }

    public function eset_internet($content)
    {
        $strStart = stripos($content, 'Username');
        $strEnd = stripos($content, 'Expiration Date');
        $result[0] = substr($content, $strStart, $strEnd - $strStart);
        $result[0] = $this->translateToEnglish($result[0]);
        $result[1] = 'Eset Internet Security';

        return $result;
    }

    public function dr_web($content)
    {
        $strStart = stripos($content, 'http://turbobit.net/');
        $content = substr($content, $strStart);
        $strEnd = stripos($content, '.html');
        $result[0] = substr($content, 0, $strEnd + 5);
        $result[0] = $this->translateToEnglish($result[0]);
        $result[1] = 'Dr.web Space Security';

        return $result;
    }

    public function bullguard_internet($content)
    {
        $content = $content->innertext();
        if (preg_match_all('/\b[0-z._%+-]+@[0-z.-]+\.[A-z]{2,6}\b/', $content, $username, PREG_OFFSET_CAPTURE)) {
            foreach ($username[0] as $item) {
                $last = $item[1];
            }
            $fist = $username[0][0][1];

            $result = substr($content, $fist, $last - $fist);
            $result = strip_tags($result);
            $result = strtr($result, ['</' => '<br>', '/>' => '<br>', '</>' => '<br>', 'p>' => '<br>']);
            $u[0] = strtr($result, [' ' => '<br>']);
            $u[1] = 'Bullguard Internet Security';

            return $u;
        }
    }

    public function ashampoo_anti($content)
    {
        if (preg_match_all('/\b[0-z]{6}-[0-z]{6}-[0-z]{6}\b/', $content, $username)) {
            $result[0] = implode('<br>', $username[0]);
            $result[0] = $this->translateToEnglish($result[0]);
            $result[1] = 'Ashampoo Antivirus';

            return $result;
        }
    }

    public function avg_anti($content)
    {
        if (preg_match_all('/\b[0-z]{4}-[0-z]{5}-[0-z]{5}-[0-z]{5}-[0-z]{5}-[0-z]{5}-[0-z]{4}\b/', $content, $username)) {
            $result[0] = implode('<br>', $username[0]);
            $result[0] = $this->translateToEnglish($result[0]);
            $result[1] = 'AVG Antivirus';

            return $result;
        }
    }

    public function avg_internet($content)
    {
        if (preg_match_all('/\b[0-z]{4}-[0-z]{5}-[0-z]{5}-[0-z]{5}-[0-z]{5}-[0-z]{5}-[0-z]{4}\b/', $content, $username)) {
            $result[0] = implode('<br>', $username[0]);
            $result[0] = $this->translateToEnglish($result[0]);
            $result[1] = 'AVG Internet Security';

            return $result;
        }
    }

    public function g_data_tc($content)
    {
        for ($i = 0; $i < 15; $i++) {
            $username = ' '. $content->find('p', $i)->outertext();
            if ( (stripos($username, 'password')) || (stripos($username, 'Contrasena')) ) {
                $result[0] = $this->translateToEnglish($username);
                $result[1] = 'G-data Total Security';

                return $result;
            }
        }
    }

    public function g_data_is($content)
    {
        for ($i = 0; $i < 15; $i++) {
            $username = $content->find('p', $i)->outertext();
            if (stripos($username, 'password')) {
                $result[0] = $this->translateToEnglish($username);
                $result[1] = 'G-data Internet Security';

                return $result;
            }
        }
    }

    public function adguard_premium($content)
    {
        $number = count($content->find("p"));
        for ($i = 0; $i < $number; $i++) {
            $username = $content->find('p', $i)->innertext();
            $username = strip_tags($username);
            if ((strlen($username) == 10) || (strlen($username) == 9) || (strlen($username) == 11)) {
                $result[0] = $this->translateToEnglish($username);
                $result[1] = 'Adguard';

                return $result;
            }
        }
    }


}
