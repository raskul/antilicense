<?php

namespace App\Http\Controllers;

use App\CozneList;
use App\License;
use Illuminate\Http\Request;

class CoznetListController extends Controller
{
    public function listGet()
    {
        $this->authorize('is-admin');

        $lists = CozneList::where('is_active', 1)->get();

        return view('coznet.list', compact('lists'));
    }

    public function listPost(Request $request)
    {
        $this->authorize('is-admin');

        CozneList::where('id', $request->id)->update(['is_active' => $request->is_active, 'antivirus_id' => $request->antivirus_id]);

        return redirect()->back();
    }

    public function updateAuto(Request $request, $id)
    {
        $this->authorize('is-admin');

        if (strlen($request->content_two_vip) >= 8 )
            License::where('antivirus_id', $id)->first()->update(['content_two_vip' => $request->content_two_vip, 'modify_user_id' => 'MeGa2']);

        return redirect()->back();
    }

}
