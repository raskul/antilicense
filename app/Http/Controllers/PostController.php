<?php

namespace App\Http\Controllers;

use Amranidev\Ajaxis\Ajaxis;
use App\Antivirus;
use App\Category;
use App\Post;
use App\Tag;
use App\User;
use App\Vote;
use Auth;
use DB;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use URL;


/**
 * Class PostController.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:14:51pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('is-writer');

        $title = 'Index - post';
        $posts = Post::orderBy('updated_at', 'desc')->paginate(25);

        return view('post.index', compact('posts', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('is-writer');

        $title = 'Create - post';
        $categories = Category::all()->pluck('name', 'id');
        $antiviruses = Antivirus::all()->pluck('name', 'id');
        $users = User::all()->pluck('name', 'id');
        $tags = Tag::pluck('name', 'id');
        $types = DB::table('types')->pluck('name', 'id');

        return view('post.create', compact('title', 'categories', 'antiviruses', 'users', 'tags', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('is-writer');

        $post = new Post();
        $post->title = $request->title;
        $post->text = $request->text;
        $post->like = $request->like;
        $post->unlike = $request->unlike;
        $post->type_id = $request->type_id;
        $post->category_id = $request->category_id;
        $post->antivirus_id = $request->antivirus_id;
        $post->user_id = $request->user_id;

        $post->save();

        $post->tags()->sync($request['tag_id']);

//        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
//        $pusher->trigger('test-channel',
//            'test-event',
//            ['message' => 'A new post has been created !!']);

        return redirect('post');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request $request
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $title = 'Show - post';

        if ($request->ajax()) {
            return URL::to('post/' . $id);
        }

        $post = Post::findOrfail($id);

        return view('post.show', compact('title', 'post'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request $request
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $title = 'Edit - post';
        if ($request->ajax()) {
            return URL::to('post/' . $id . '/edit');
        }

        $categories = Category::all()->pluck('name', 'id');
        $antiviruses = Antivirus::all()->pluck('name', 'id');
        $users = User::all()->pluck('name', 'id');

        $post = Post::findOrfail($id);

        $types = DB::table('types')->pluck('name', 'id');

        $tags = Tag::pluck('name', 'id');

        if (Gate::allows('is-editor'))
            return view('post.edit', compact('title', 'post', 'categories', 'antiviruses', 'users', 'tags', 'types'));

        if (Gate::allows('is-writer') || Gate::allows('is-post-owner', $post))
            return view('post.editLite', compact('post'));

        $this->authorize('is-writer');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $post = Post::findOrfail($id);

        $this->authorize('is-writer');
        $this->authorize('is-post-owner', $post);

        $post->title = $request->title;
        $post->text = $request->text;
        $post->like = $request->like;
        $post->unlike = $request->unlike;

        if (!is_null($request->type_id)) {
            $post->type_id = $request->type_id;
        } else {
            $post->type_id = Post::where('id', $id)->pluck('type_id')->first();
        }
        $post->category_id = $request->category_id;
        $post->antivirus_id = $request->antivirus_id;
        $post->user_id = $request->user_id;

        $post->save();

        $post->tags()->sync($request['tag_id']);

        return redirect('post');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request $request
     * @return  String
     */
    public function DeleteMsg($id, Request $request)
    {
        $msg = Ajaxis::BtDeleting('هشدار', 'آیا اطمینان دارید که میخواهید پاک شود؟', '/post/' . $id . '/delete');

        if ($request->ajax()) {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrfail($id);

        $this->authorize('is-writer');
        $this->authorize('is-post-owner', $post);

        $post->delete();

        return URL::to('post');
    }

    public function storeCPost($id, Request $request)
    {
        if (Auth::check()) {
            $inputs['text'] = $request->text;
            $inputs['user_id'] = auth()->user()->id;
            $inputs['antivirus_id'] = $id;
            $inputs['type_id'] = 2;
            auth()->user()->posts()->create($inputs);

        }
//        dd(auth()->user()->id);
//        dd($id) ;
//        $inputs = $request->content ;

        return redirect()->back();
    }

    public function updateCPost($id, Request $request)
    {
        if (Auth::check()) {
            $inputs['text'] = $request->text;
            $inputs['user_id'] = auth()->user()->id;
//            $inputs['antivirus_id'] = $id;
            $inputs['type_id'] = 2;
            Post::where('id', $id)->update($inputs);
//            auth()->user()->posts()->update($inputs);

        }
//        dd(auth()->user()->id);
//        dd($id) ;
//        $inputs = $request->content ;

        return redirect()->back();
    }

    public function votePositive(Post $post)
    {
        if (!Auth::check())
            return message('ابتدا باید ثبت نام کنید. ثبت نام شما کمتر از 10 ثانیه طول خواهد کشید.', 'register', 'ثبت نام', 'پیغام سیستم', 'success');

        $user_id = auth()->user()->id;
        $post_id = $post->id;
        $able = Vote::where('user_id', $user_id)->where('post_id', $post_id)->where('type', 1)->first();

        if ($post->user_id == $user_id)
            return message('شما از دیدگاه خودتان نمیتوانید تشکر کنید.');

        if ($able){
            return message('شما از این دیدگاه قبلا تشکر کرده اید.');
        }else{
            Vote::create(['type' => 1, 'post_id' => $post_id, 'user_id' => $user_id]);
        }

        $post->update(['like' => $post['like'] + 1]);
        $post_vote = $post->user()->pluck('like')->first();
        User::where('id', $post->user_id)->update(['like' => $post_vote + 1]);

//        $post->user()->first()->update(['like' => 100 ]);//chera update nameshe? chera???
        return redirect()->back();
    }

    public function voteNegative(Post $post)
    {
        if (!Auth::check())
            return message('ابتدا باید ثبت نام کنید. ثبت نام شما کمتر از 10 ثانیه طول خواهد کشید.', 'register', 'ثبت نام', 'پیغام سیستم', 'success');

        $user_id = auth()->user()->id;
        $post_id = $post->id;
        $able = Vote::where('user_id', $user_id)->where('post_id', $post_id)->where('type', -1)->first();

        if ($post->user_id == $user_id)
            return message('شما نمیتوانید دیدگاه خود را گزارش دهید.');

        if ($able){
            return message('شما این دیدگاه را قبلا گزارش داده اید.');
        }else{
            Vote::create(['type' => -1, 'post_id' => $post_id, 'user_id' => $user_id]);
        }

        $post->update(['unlike' => $post['unlike'] - 1]);
        $post_vote = $post->user()->pluck('unlike')->first();
        User::where('id', $post->user_id)->update(['unlike' => $post_vote - 1]);

        //$post->user()->update(['unlike' => $post_vote - 1]);//chera update nameshe? chera???
        return message('گزارش شما ثبت شد. با تشکر فراوان بابت این که این مورد را گزارش دادید.');
//        return redirect()->back();
    }
}
