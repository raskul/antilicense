<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Log.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:31:34pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Log extends Model
{
    protected $fillable = [
        'ip', 'url', 'call_back_url', 'os', 'browser', 'username'
    ];
	
    protected $table = 'logs';

	
}
