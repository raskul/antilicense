<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CozneBlackList extends Model
{
    protected $fillable = [
        'name',
    ];
}
