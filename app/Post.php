<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:14:51pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Post extends Model
{

	protected $fillable = [
	    'id', 'title', 'text', 'like', 'unlike', 'type_id', 'category_id', 'antivirus_id'
    ];
    protected $table = 'posts';

	
	public function category()
	{
		return $this->belongsTo('App\Category','category_id');
	}

	
	public function antivirus()
	{
		return $this->belongsTo('App\Antivirus','antivirus_id');
	}

	
	public function user()
	{
		return $this->belongsTo(User::class);
	}

    public function type()
    {
        return $this->belongsTo(Type::class);
	}

	

	/**
     * tag.
     *
     * @return  \Illuminate\Support\Collection;
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Assign a tag.
     *
     * @param  $tag
     * @return  mixed
     */
    public function assignTag($tag)
    {
        return $this->tags()->attach($tag);
    }
    /**
     * Remove a tag.
     *
     * @param  $tag
     * @return  mixed
     */
    public function removeTag($tag)
    {
        return $this->tags()->detach($tag);
    }


}
