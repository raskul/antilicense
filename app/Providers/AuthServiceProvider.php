<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('is-vip', function ($user){
            if($user->role_id == 2 || $user->role_id == 3 || $user->role_id == 4 || $user->role_id == 5)
                return true;
        });

        Gate::define('is-writer', function ($user){
            if($user->role_id == 3 || $user->role_id == 4 || $user->role_id == 5)
                return true;
        });

        Gate::define('is-editor', function ($user){
            if($user->role_id == 4 || $user->role_id == 5)
                return true;
        });

        Gate::define('is-admin', function ($user){
            if($user->role_id == 5)
                return true;
        });

        Gate::define('is-post-owner', function ($user, $post){
            if( ($user->id == $post->user_id) || ($user->role_id == 5) )
                return true;
        });

        Gate::define('is-comment-owner', function ($user, $comment){
            if( ($user->id == $comment->user_id) || ($user->role_id == 5) )
                return true;
        });

        Gate::define('is-profile-owner', function ($user, $user_id){
            if( ($user->id == $user_id) || ($user->role_id == 5))
                return true;
        });
    }
}
