<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zarin extends Model
{
    protected $fillable = [
        'message', 'ref_id', 'email', 'amount', 'username', 'status',
    ];
}
