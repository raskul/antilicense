<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoznePost extends Model
{
    protected $fillable = [
        'title', 'description', 'username', 'link', 'category', 'dc_creator', 'guid', 'pub_date', 'antivirus_id', 'type'
    ];
}
