<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tag.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:26:08pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Tag extends Model
{
	
	
    protected $table = 'tags';

	

	/**
     * post.
     *
     * @return  \Illuminate\Support\Collection;
     */
    public function posts()
    {
        return $this->belongsToMany('App\Post');
    }

    /**
     * Assign a post.
     *
     * @param  $post
     * @return  mixed
     */
    public function assignPost($post)
    {
        return $this->posts()->attach($post);
    }
    /**
     * Remove a post.
     *
     * @param  $post
     * @return  mixed
     */
    public function removePost($post)
    {
        return $this->posts()->detach($post);
    }

}
