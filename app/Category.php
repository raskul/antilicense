<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:10:48pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Category extends Model
{
	
	
    protected $table = 'categories';

	
	public function antivirus()
	{
		return $this->belongsTo('App\Antivirus','antivirus_id');
	}

	
}
