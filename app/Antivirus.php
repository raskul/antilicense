<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Antivirus.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:06:36pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Antivirus extends Model
{
	
    protected $fillable = [
        'name', 'name_fa', 'image'
    ];
    protected $table = 'antiviruses';

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function license()
    {
        return $this->hasMany(License::class);
    }
	
}
