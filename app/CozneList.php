<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CozneList extends Model
{
    protected $fillable = [
        'name','antivirus_id', 'is_active'
    ];
}
