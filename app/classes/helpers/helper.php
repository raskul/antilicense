<?PHP
function message($messageString, $route = null, $buttonText = 'بازگشت', $title = 'پیغام سیستم', $type = 'primary'){
    $message = new stdClass();
    $message->text = $messageString;
    $message->title = $title;
    $message->type = $type;
    $message->button = null;
    return view('index.message', compact('message', 'route', 'buttonText'));
}
?>

