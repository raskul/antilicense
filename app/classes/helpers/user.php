<?php

namespace App\classes\helpers;

use Auth;

class User
{
    function getUserInfo()
    {
        $username = null;
        if (Auth::check())
            $username = auth()->user();

        return $username;
    }
}
