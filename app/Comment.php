<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Comment.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:21:30pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Comment extends Model
{
	
	
    protected $table = 'comments';

	
	public function post()
	{
		return $this->belongsTo('App\Post','post_id');
	}

	
	public function user()
	{
		return $this->belongsTo('App\User','user_id');
	}

	
}
