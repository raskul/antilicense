<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vote.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:28:06pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Vote extends Model
{
	
	
    protected $table = 'votes';

	protected $fillable = [
	    'type', 'user_id', 'post_id'
    ];
}
