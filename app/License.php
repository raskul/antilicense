<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class License.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:24:34pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class License extends Model
{


    protected $table = 'licenses';

    protected $fillable = [
        'antivirus_id',
        'modify_user_id',
        'content_five',
        'content_four_vip',
        'content_four',
        'content_three_vip',
        'content_three',
        'content_two_vip',
        'content_two',
        'content_one_vip',
        'content_one',
        'updated_at',
        'original_post_date',
    ];


    public function antivirus()
    {
        return $this->belongsTo('App\Antivirus', 'antivirus_id');
    }


}
