<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([ 'name'=>'آموزش' ]);
        DB::table('types')->insert([ 'name'=>'دیدگاه' ]);
        DB::table('types')->insert([ 'name'=>'دیدگاه منتخب' ]);
        DB::table('types')->insert([ 'name'=>'دانلود' ]);
        DB::table('types')->insert([ 'name'=>'درباره' ]);
        DB::table('types')->insert([ 'name'=>'نقاط قوت' ]);
        DB::table('types')->insert([ 'name'=>'مقایسه' ]);
        DB::table('types')->insert([ 'name'=>'لایسنس' ]);
        DB::table('types')->insert([ 'name'=>'پست' ]);
    }
}
