<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([ 'name'=>'یوزر معمولی' ]);
        DB::table('roles')->insert([ 'name'=>'یوزر ویژه' ]);
        DB::table('roles')->insert(['name'=>'نویسنده' ]);
        DB::table('roles')->insert(['name'=>'ویرایشگر' ]);
        DB::table('roles')->insert(['name'=>'ادمین' ]);
    }
}
