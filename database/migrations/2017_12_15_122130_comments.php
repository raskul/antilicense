<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Comments.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:21:30pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Comments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('comments',function (Blueprint $table){

        $table->increments('id');
        
        $table->Text('text');
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('post_id')->unsigned()->nullable();
        $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade')->onUpdate('cascade');
        
        $table->integer('user_id')->unsigned()->nullable();
        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}
