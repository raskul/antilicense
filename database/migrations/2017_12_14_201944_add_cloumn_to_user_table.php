<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCloumnToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->unsignedInteger('role_id')->unsigned()->default('1');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('active')->nullable()->default(1);
            $table->string('image')->nullable();
            $table->text('about')->nullable();
            $table->integer('like')->nullable()->default(0);
            $table->integer('unlike')->nullable()->default(0);
            $table->dateTime('vip_end_date')->nullable();
            $table->text('report')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('roles');
            $table->dropColumn('active');
            $table->dropColumn('image');
            $table->dropColumn('about');
            $table->dropColumn('like');
            $table->dropColumn('unlike');
            $table->dropColumn('vip');
            $table->dropColumn('report');
        });
    }
}
