<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class Posts.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:14:51pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Posts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {

            $table->increments('id');

            $table->String('title')->nullable();

            $table->longText('text');

            $table->integer('like')->nullable()->default(0);

            $table->integer('unlike')->nullable()->default(0);

            $table->unsignedInteger('type_id')->unsigned();
            $table->foreign('type_id')->references('id')->on('types')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('actived')->nullable()->default(1);

            /**
             * Foreignkeys section
             */

            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('antivirus_id')->unsigned()->nullable();
            $table->foreign('antivirus_id')->references('id')->on('antiviruses')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');


            $table->timestamps();


            // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
