<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoznePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cozne_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->text('link')->nullable();
            $table->text('category')->nullable();
            $table->text('dc_creator')->nullable();
            $table->text('guid')->nullable();
            $table->text('username')->nullable();
            $table->integer('pub_date')->nullable();
            $table->integer('antivirus_id')->nullable();
            $table->string('type')->nullable();
            $table->integer('published')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cozne_posts');
    }
}
