<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZarinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zarins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amount');
            $table->string('email')->nullable();
            $table->string('ref_id')->nullable();
            $table->string('message')->nullable();
            $table->string('username')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zarins');
    }
}
