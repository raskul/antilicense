<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Antiviruses.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:06:36pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Antiviruses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('antiviruses',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('name');

        $table->string('name_fa')->nullable();

        $table->String('image')->nullable();

        $table->integer('order')->nullable();

        $table->integer('is_active')->nullable()->default(1);


        /**
         * Foreignkeys section
         */
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('antiviruses');
    }
}
