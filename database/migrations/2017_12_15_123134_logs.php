<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Logs.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:31:34pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Logs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('logs',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('ip')->nullable();
        
        $table->String('url')->nullable();
        
        $table->String('call_back_url')->nullable();
        
        $table->String('os')->nullable();
        
        $table->String('browser')->nullable();

        $table->string('username')->nullable();
        
        /**
         * Foreignkeys section
         */
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('logs');
    }
}
