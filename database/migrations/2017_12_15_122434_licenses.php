<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Licenses.
 *
 * @author  The scaffold-interface created at 2017-12-15 12:24:34pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Licenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('licenses',function (Blueprint $table){

        $table->increments('id');
        
        $table->text('content_one')->nullable();
        
        $table->text('content_one_vip')->nullable();
        
        $table->text('content_two')->nullable();
        
        $table->text('content_two_vip')->nullable();
        
        $table->text('content_three')->nullable();
        
        $table->text('content_three_vip')->nullable();
        
        $table->text('content_four')->nullable();
        
        $table->text('content_four_vip')->nullable();

        $table->text('content_five')->nullable();
        
        $table->String('modify_user_id')->nullable();

        $table->integer('original_post_date')->nullable();
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('antivirus_id')->unsigned()->nullable();
        $table->foreign('antivirus_id')->references('id')->on('antiviruses')->onDelete('cascade')->onUpdate('cascade');
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('licenses');
    }
}
